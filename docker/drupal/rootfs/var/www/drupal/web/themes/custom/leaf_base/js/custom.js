(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.leaf_base = {
    attach: function(context, settings) {
      if(Drupal.behaviors.leaf_base.loaded) { return; }
      Drupal.behaviors.leaf_base.loaded = true;
      var $colnav = $('.collapsible-navigation');

      // Get display from query parameter.
      var display = getUrlParameter('display');
      if (display) {
        // Add active class on click of display icon.
        var selector = '.view-solr-search-collection-block .display_selector__container a.' + display;
        $(selector).addClass('is-active');

        var selector2 = '.view-solr-search-group-block .display_selector__container a.' + display;
        $(selector2).addClass('is-active');

        var selector3 = '.view-solr-search-content .display_selector__container a.' + display;
        $(selector3).addClass('is-active');
      }
      else {
        // Add active class on first display icon.
        $('.view-solr-search-content .display_selector__container a:first, .view-solr-search-group-block .display_selector__container a:first, .view-solr-search-collection-block .display_selector__container a:first').addClass('is-active');
      }

      $('span#search-icon').on('click', function() {
        $colnav.removeClass('main account');
        if($('.collapsible-navigation.open.search').length > 0) {
          $colnav.removeClass('open');
        } else {
          $colnav.addClass('open');
        }
        $colnav.toggleClass('search');
      });

      $('button.navbar-toggler').on('click', function() {
        $colnav.removeClass('search account');
        if($('.collapsible-navigation.open.main').length > 0) {
          $colnav.removeClass('open');
        } else {
          $colnav.addClass('open');
        }
        $colnav.toggleClass('main');
      });

      $('span#user-icon').on('click', function() {
        $colnav.removeClass('main search');
        if($('.collapsible-navigation.open.account').length > 0) {
          $colnav.removeClass('open');
        } else {
          $colnav.addClass('open');
        }
        $colnav.toggleClass('account');
      });

      // Only show the project gallery filters when all projects are visible on the page.
      $('.view-footer button').on('click', function(ev) {
        ev.preventDefault();
        $('.pg-footer').hide();
        $('.hidden-item').removeClass('hidden-item');
        $('body').addClass('show-pg-filters');
      });

      if($('.slider-wrapper').length > 0) {
        $('.slider-wrapper').slick({
          dots: false,
          mobileFirst: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          slidesToScroll: 1
        });
      }

      $('.pager__item').each(function(){
        var pagerStr = $(this).text();
        pagerNum = pagerStr.match(/\d+/)[0];
        pagerStr = pagerStr.replace(/\d+/, "<span class='border-around-text'>" + pagerNum + "</span>");
        $(this).html(pagerStr);
      });

      $(".filter-box>.hidden").each(function(){
        $(this).parent().addClass('hidden');
      });

      $(document).on('change', '.main-content .block-views-exposed-filter-blocksolr-search-content-page-1 form .js-form-item.js-form-item-sort-bef-combine select', function (ev) {
        $("#edit-submit-solr-search-content--2").click();
      });

      $(document).on('change', '.block-views-exposed-filter-blocksolr-search-group-block-page-1 form .js-form-item.js-form-item-group-sort-bef-combine select', function (ev) {
        $("#edit-submit-solr-search-group-block--2").click();
      });
    }
  }
})(jQuery, Drupal, drupalSettings);

function getUrlParameter(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
  results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

