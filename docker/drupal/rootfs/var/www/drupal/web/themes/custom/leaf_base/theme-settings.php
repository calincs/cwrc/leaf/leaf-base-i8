<?php

/**
 * @file
 * Custom theme settings for LEAF themes.
 */

/**
 * WAP Theme Settings.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function leaf_base_form_system_theme_settings_alter(&$form, FormStateInterface $form_state, $form_id = NULL) {
  $form['theme_switcher'] = [
    '#type' => 'details',
    '#title'  => t('Theme Switcher Settings'),
    '#open' => FALSE,
  ];

  $form['theme_switcher']['theme_brand'] = [
    '#type' => 'select',
    '#title' => t('Select current theme branding'),
    '#description' => t('Choose which theme branding to display. Each have different color scheme and logos'),
    '#options' => [
      'LEAF' => t('LEAF'),
      'bucknell' => t('Bucknell'),
      'newcastle' => t('Newcastle'),
      'CWRC' => t('CWRC'),
    ],
    '#default_value'  => theme_get_setting('theme_brand'),
  ];

  $form['homepage_banner'] = [
    '#type' => 'details',
    '#title'  => t('Homepage Banner Configurations'),
    '#open' => FALSE,
  ];

  $form['homepage_banner']['banner_heading'] = [
    '#type' => 'textfield',
    '#title'  => t('Banner Heading'),
    '#default_value'  => theme_get_setting('banner_heading'),
    '#description'  => t('Heading text in the homepage banner.'),
  ];

  $form['homepage_banner']['banner_tagline'] = [
    '#type' => 'textfield',
    '#title'  => t('Banner Tagline'),
    '#default_value'  => theme_get_setting('banner_tagline'),
    '#description'  => t('Tagline in the homepage banner under heading text.'),
  ];

  $form['homepage_banner']['banner_button'] = [
    '#type'  => 'details',
    '#title'  => t('Banner CTA Button'),
    '#open' => TRUE,
  ];

  $form['homepage_banner']['banner_button']['banner_btn_text'] = [
    '#type' => 'textfield',
    '#title'  => t('Banner Button Text'),
    '#default_value'  => theme_get_setting('banner_btn_text'),
    '#description'  => t('Site short name for banner button.'),
  ];

  $form['homepage_banner']['banner_button']['banner_btn_url'] = [
    '#type' => 'textfield',
    '#title'  => t('Banner Button URL'),
    '#default_value'  => theme_get_setting('banner_btn_url'),
    '#description'  => t('URL to redirect when button is clicked.'),
  ];

  $form['homepage_banner']['banner_background'] = [
    '#type' => 'managed_file',
    '#title'  => t('Banner Background'),
    '#default_value'  => theme_get_setting('banner_background'),
    '#description'  => t('Background image for homepage banner. Please use 1440x526 image size.'),
    '#upload_location'  => 'public://banner/',
    '#upload_validators'  => [
      'file_validate_extensions'  => ['png jpg jpeg'],
    ],
  ];
  $form['#submit'][] = 'leaf_base_theme_settings_submit';
}

/**
 * Custom submit handler for theme settings form.
 */
function leaf_base_theme_settings_submit(&$form, FormStateInterface $form_state, $form_id = NULL) {
  $banner_file = $form_state->getValue('banner_background');
  if (!empty($banner_file)) {
    // File load banner image.
    $file = \Drupal\file\Entity\File::load(reset($banner_file));
    if (isset($file)) {
      $file->setPermanent();
      $file->save();
    }
  }
}
