## Prerequisites

- This works on Windows, macOS and Linux.
- Node Package Manager and Gulp are required. Make sure you can run `gulp -v` and `npm -v`.
- You can get Node at [nodejs.org](https://nodejs.org), then install gulp using `npm install gulp-cli -g`

## Getting started
*`$ cd` to your theme folder terminal*

1. Run `$ npm install` *Install NPM dependencies*
2. *This next script cleans and builds SASS as you save.* `gulp`

Press `ctrl + c` to stop compiler
