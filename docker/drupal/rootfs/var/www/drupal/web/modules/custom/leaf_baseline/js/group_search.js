(function ($, Drupal, drupalSettings) {

    //Drupal behaviours
    Drupal.behaviors.group_search = {
      attach: function (context, settings) {
        $(document).ready(function () {
            $(document).on('change', '#edit-search-parent-node', function (ev) {
                var value = $(this).val();
                newString = value.replace(/\(.*\)/g, '').replace(/"/g, '').trim();
                $('#edit-parent-title').val(newString);
            });
        });
      }
    };

  })(jQuery, Drupal, drupalSettings);
