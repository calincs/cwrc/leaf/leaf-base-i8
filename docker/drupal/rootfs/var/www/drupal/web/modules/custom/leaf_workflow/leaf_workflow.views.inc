<?php

/**
 * @file
 * The module file.
 */

/**
 * Implements hook_views_data().
 */
function leaf_workflow_views_data() {
  $data['views']['table']['group'] = t('Leaf Workflow Fields');
  $data['views']['table']['join'] = [
    // #global is a special flag which allows a table to appear all the time.
    '#global' => [],
  ];

  $data['views']['workflow_activity_field'] = [
    'title' => t('Workflow Activity'),
    'help' => t('Leaf workflow activity field.'),
    'field' => [
      'id' => 'workflow_activity_field',
    ],
  ];

  $data['views']['workflow_visibility_field'] = [
    'title' => t('Workflow Visibility'),
    'help' => t('Leaf workflow visibility field.'),
    'field' => [
      'id' => 'workflow_visibility_field',
    ],
  ];

  $data['views']['workflow_status_field'] = [
    'title' => t('Workflow Status'),
    'help' => t('Leaf workflow status field.'),
    'field' => [
      'id' => 'workflow_status_field',
    ],
  ];

  $data['views']['workflow_user_field'] = [
    'title' => t('Workflow User Assignee'),
    'help' => t('Leaf workflow user field.'),
    'field' => [
      'id' => 'workflow_user_field',
    ],
  ];

  $data['views']['workflow_user_revision_revert_link'] = [
    'title' => t('Workflow revision revert link'),
    'help' => t('Leaf workflow revision revert field.'),
    'field' => [
      'id' => 'workflow_user_revision_revert_link',
    ],
  ];

  return $data;
}
