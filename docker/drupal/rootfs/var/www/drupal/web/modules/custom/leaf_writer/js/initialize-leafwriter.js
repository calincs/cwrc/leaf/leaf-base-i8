/**
 * @file
 * Initialize the leaf writer library.
 */
const schemas = [
  {
    id: 'event',
    name: 'Orlando Events',
    mapping: 'orlando',
    rng: [
      'https://cwrc.ca/schemas/orlando_event.rng',
      'https://raw.githubusercontent.com/cwrc/CWRC-Schema/master/schemas/orlando_event.rng',
    ],
    css: [
      'https://cwrc.ca/templates/css/orlando_v2_cwrc-writer.css',
      'https://raw.githubusercontent.com/cwrc/CWRC-Schema/master/templates/css/orlando_v2_cwrc-writer.css',
    ],
  },
  {
    id: 'cwrcTeiLite',
    name: 'CWRC TEI Lite',
    mapping: 'tei',
    rng: [
      'https://cwrc.ca/schemas/cwrc_tei_lite.rng',
      'https://raw.githubusercontent.com/cwrc/CWRC-Schema/master/schemas/cwrc_tei_lite.rng',
    ],
    css: [
      'https://cwrc.ca/templates/css/tei.css',
      'https://raw.githubusercontent.com/cwrc/CWRC-Schema/master/templates/css/tei.css',
    ],
  },
  {
    id: 'cwrcEntry',
    name: 'CWRC Entry',
    mapping: 'cwrcEntry',
    rng: [
      'https://cwrc.ca/schemas/cwrc_entry.rng',
      'https://raw.githubusercontent.com/cwrc/CWRC-Schema/master/schemas/cwrc_entry.rng',
    ],
    css: [
      'https://cwrc.ca/templates/css/cwrc.css',
      'https://raw.githubusercontent.com/cwrc/CWRC-Schema/master/templates/css/cwrc.css',
    ],
  },
  {
    id: 'reed',
    name: 'REED',
    mapping: 'tei',
    rng: [
      'https://cwrc.ca/islandora/object/cwrc%3A5d5159ce-8710-4717-b977-cc528dedc25e/datastream/SCHEMA/view',
      'https://raw.githubusercontent.com/LEAF-VRE/schemas/main/reed/out/reed.rng',
    ],
    css: ['https://cwrc.ca/templates/css/tei.css'],
  },
];

((Drupal, once, drupalSettings) => {
  const initializeElement = function (element) {
    const config = JSON.parse(element.dataset.leafWriterConfig);
    const xmlInput = element.querySelector('.leaf-writer--xml-input');
    const editor = new Leafwriter.Leafwriter(element.querySelector('.leaf-writer--container'))
    const leafwriterReadOnly = drupalSettings.leafWriter ? (drupalSettings.leafWriter.readOnly??false) : false;
    editor.init({
      document: {
        url: config.xml_file_url,
        xml: (xmlInput.value).trim()
      },
      settings: {
        baseUrl: '/libraries/leafwriter/dist',
        readonly: leafwriterReadOnly,
        schemas,
      },
      user: {
        name: config.user.name,
        uri: config.user.id
      }
    });

    // Update the xml value stored in the xml input on form submit.
    const form = xmlInput.closest('form');
    if (form) {
      form.addEventListener('submit', (e) => {
        editor.getContent().then((xml) => {
          xmlInput.value = xml;
          editor.setContentHasChanged(false);
        });
      });
    }
  };

  /**
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.leafWriterInitializer = {
    attach: function (context) {
      const elements = once('leaf-writer-initialized', '[data-leaf-writer-config]', context);
      // `elements` is always an Array.
      elements.forEach(initializeElement);
    }
  };
})(Drupal, once, drupalSettings);
