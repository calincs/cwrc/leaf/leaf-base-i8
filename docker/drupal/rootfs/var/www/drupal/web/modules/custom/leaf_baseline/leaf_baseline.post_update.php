<?php

/**
 * A hook_post_update_NAME() hook to switch profile to minimal. 
 */ 
function leaf_baseline_post_update_switch_profile() {
    // Code goes here.
  $state = \Drupal::service('state');
  $profile_to_install = 'minimal';
  $profile_to_remove = \Drupal::installProfile();
  if ($profile_to_remove === $profile_to_install) {
    return (string) t('The installed profile is already @new', ['@new' => $profile_to_install]);
  }

  // Forces ExtensionDiscovery to rerun for profiles.
  $state->delete('system.profile.files');
   // Set the profile in configuration.
  $config_factory = \Drupal::service('config.factory');
  $extension_config = $config_factory->getEditable('core.extension');
  $extension_config->set('profile', $profile_to_install)->save();

  drupal_flush_all_caches();

  // Install profiles are also registered as enabled modules.
  // Remove the old profile and add in the new one.
  $extension_config->clear("module.{$profile_to_remove}")->save();
  // The install profile is always given a weight of 1000 by the core
  // extension system.
  $extension_config->set("module.$profile_to_install", 1000)->save();

  // Updating the schema to match the updated config.
  $key_value = \Drupal::service('keyvalue');
  $key_value->get('system.schema')->delete($profile_to_remove);
  $key_value->get('system.schema')->set($profile_to_install, 8000);
  drupal_flush_all_caches();
  return (string) t('Successfully switched profile from @old to @new', ['@new' => $profile_to_install, '@old' => $profile_to_remove]);

}