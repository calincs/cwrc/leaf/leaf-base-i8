<?php

namespace Drupal\leaf_content_lock\Controller;

use Drupal\Core\Database\Connection;
use Drupal\Core\Controller\ControllerBase;
use Drupal\content_lock\ContentLock\ContentLock;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Entity\EntityInterface;


/**
 * Class LeafContentlockEmailController.
 *
 * @package Drupal\leaf_content_lock\Controller
 */
class LeafContentlockEmailController extends ControllerBase {

  /**
   * Content lock service.
   *
   * @var \Drupal\content_lock\ContentLock\ContentLock
   */
  protected $lockService;

  /**
   * Constructs a Merge object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   A Connection object.
   * @param string $table
   *   Name of the table to associate with this query.
   * @param array $options
   *   Array of database options.
   */

  protected $database;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current_user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   *   The current_user service.
   */
  protected $currentUser;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * EntityBreakLockForm constructor.
   *
   * @param \Drupal\content_lock\ContentLock\ContentLock $lock_service
   *   Content lock service.
   */
  public function __construct(ContentLock $lock_service, Connection $database, MailManagerInterface $mailManager, RequestStack $request_stack, AccountProxyInterface $currentUser, EntityTypeManagerInterface $entityManager, MessengerInterface $messenger) {
    $this->lockService = $lock_service;
    $this->database = $database;
    $this->mailManager = $mailManager;
    $this->requestStack = $request_stack;
    $this->currentUser = $currentUser;
    $this->entityManager = $entityManager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('content_lock'),
      $container->get('database'),
      $container->get('plugin.manager.mail'),
      $container->get('request_stack'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('messenger'),
    );
  }

  /**
   * Custom callback for the create lock route.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *
   * @see \Drupal\content_lock\Routing\ContentLockRoutes::routes()
   */
  public function sendEmail(EntityInterface $entity) {
    $leaf_content_lock_config = \Drupal::config('leaf_content_lock.settings');
    $admin_email_content = $leaf_content_lock_config->get('leaf_content_lock_email');
    $token_data = array(  //assigns the current node data to the 'node' key in the $data array; the node key is recognized by the Token service
      'content_url' => $entity,
      'content_title' => $entity,
    );
    $admin_email_content = \Drupal::token()->replace($admin_email_content, $token_data, array('sanitize' => FALSE));
    $request = $this->requestStack->getCurrentRequest();
    $entity_id = $entity->id();
    $entity_type = $entity->getEntityType()->id();
    $langcode = $request->query->get('langcode');
    $form_op = $request->query->get('form_op');
    $destination = $entity->toUrl()->toString();
    $uid = $this->currentUser()->id();
    $user = $entity->getOwner();
    $email = $user->getEmail();
    $result = $this->database->merge('leaf_content_lock_release_email')
      ->key([
        'entity_id' => $entity_id,
        'entity_type' => $entity_type,
        'langcode' => $langcode,
        'form_op' => $form_op,
      ])
      ->fields([
        'entity_id' => $entity_id,
        'entity_type' => $entity_type,
        'langcode' => $langcode,
        'form_op' => $form_op,
        'requested_uid' => $uid,
        'timestamp' => time(),
      ])
      ->execute();

    $mailManager = $this->mailManager;
    $params['message'] = $admin_email_content;
    $site_config = \Drupal::config('system.site');
    $params['title'] = $this->t('Request to break content editing lock: ' . $site_config->get('name'));
    $langcode = $langcode;
    $send = true;

    $result = $mailManager->mail('leaf_content_lock', 'lock_request_mail', $email, $langcode, $params, NULL, $send);
    if ($result['result'] != true) {
      $message = t('There was a problem sending your email notification to @email.', array('@email' => $email));
      // drupal_set_message($message, 'error');
      $this->messenger->addWarning($message);
    }
    else {
      $message = t('An email notification has been sent to @email ', array('@email' => $email));
      $this->messenger->addMessage($message);
    }

    \Drupal::service('request_stack')->getCurrentRequest()->query->set('destination', $destination);

  }

}
