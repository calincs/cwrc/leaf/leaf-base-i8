<?php

/**
 * @file
 * The module file.
 */

/**
 * Implements hook_views_data().
 */
function leaf_baseline_views_data() {
  $data['views']['table']['group'] = t('Default Thumbnail');
  $data['views']['table']['join'] = [
    '#global' => [],
  ];

  $data['views']['default_thumbnail_field'] = [
    'title' => t('Default Thumbnail'),
    'help' => t('The Default Thumbnail for repository item nodes.'),
    'field' => [
      'title' => t('Default Thumbnail'),
      'help' => t('Default thumbnail field.'),
      'id' => 'default_thumbnail_field',
    ],
  ];

  $data['views']['current_logged_in_group_user_role_field'] = [
    'title' => t('Current Logged In User Group Role'),
    'help' => t('Current Logged In User Group Role.'),
    'field' => [
      'title' => t('Current Logged In User Group Role'),
      'help' => t('Current Logged In User Group Role field.'),
      'id' => 'current_logged_in_group_user_role_field',
    ],
  ];

  $data['views']['group_member_timezone'] = [
    'title' => t('Group member timezone field'),
    'help' => t('Group member timezone field.'),
    'field' => [
      'title' => t('Group member timezone field'),
      'help' => t('Group member timezone field.'),
      'id' => 'group_member_timezone',
    ],
  ];

  return $data;
}
