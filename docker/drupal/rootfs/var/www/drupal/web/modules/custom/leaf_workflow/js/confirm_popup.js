(function ($, Drupal) {
    //Popup in function so we can pass content title to the popup
    function Confirm_popup() {
      var content = "<div>Are you sure you don't want to save this workflow record?</div>";
      confirmationDialog = Drupal.dialog(content, {
        dialogClass: 'confirm-dialog',
        resizable: false,
        closeOnEscape: false,
        width: 600,
        buttons: [
          {
            text: 'Yes, Cancel',
            click: function () {
              $(this).dialog('close');
              $('.view-id-content_revision_custom .about-table-content-wrapper.history-form .about-table-main').removeClass('show', 1000, "easeInBack");
              $('.path-node.page-view-manage-about .about-table-content-wrapper.history-form h3 a').addClass('collapsed');
            }
          },
          {
            text: 'No, Save it',
            class: 'button--primary button',
            click: function () {
              $('#edit-submit').click();
            }
          }
        ],
      });
      confirmationDialog.showModal();
    }

    // Call to function to open popup.
    $("#edit-cancel").click(function (e) {
      e.preventDefault();
      Confirm_popup();
    });

    Drupal.behaviors.workflowConfirmPopup = {
      attach: function (context, settings) {
        $('#workflow-ajax-wrapper').insertAfter('.action-wrapper');

        // Collapse history section on saving new revision.
        if ($(".leaf-workflow-status-msg")[0]) {
          $('.view-id-content_revision_custom .about-table-content-wrapper.history-form .about-table-main').removeClass('show', 1000, "easeInBack");
          $('.path-node.page-view-manage-about .about-table-content-wrapper.history-form h3 a').addClass('collapsed');
          $('div').removeClass('leaf-workflow-status-msg');
        }
      }
    }
})(jQuery, Drupal);
