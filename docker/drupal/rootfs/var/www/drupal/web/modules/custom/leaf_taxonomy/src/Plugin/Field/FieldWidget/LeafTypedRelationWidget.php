<?php

namespace Drupal\leaf_taxonomy\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\controlled_access_terms\Plugin\Field\FieldWidget\TypedRelationWidget;

/**
 * Plugin implementation of the typed note widget.
 *
 * @FieldWidget(
 *   id = "leaf_typed_relation_default",
 *   label = @Translation("LEAF Typed Relation Widget"),
 *   field_types = {
 *     "typed_relation"
 *   }
 * )
 */
class LeafTypedRelationWidget extends TypedRelationWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $widget = parent::formElement($items, $delta, $element, $form, $form_state);

    $widget['#attached']['library'][] = 'leaf_taxonomy/relation-widget-forms';

    return $widget;
  }

}
