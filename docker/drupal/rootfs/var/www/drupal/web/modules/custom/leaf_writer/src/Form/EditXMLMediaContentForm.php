<?php

namespace Drupal\leaf_writer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\Media;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;

/**
 * Class EditXMLMediaContentForm.
 *
 * @package Drupal\leaf_writer\Form
 */
class EditXMLMediaContentForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'leafwriter_media_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm($form, FormStateInterface $form_state, $node = NULL) {
    $lock_service = \Drupal::service('content_lock');
    $entity = $node;
    $entity_type = $entity->getEntityTypeId();
    $user = \Drupal::currentUser();
    $node_id = $node->id();
    // Get Media Item of current Node with fits_technical_metadata reference.
    $mediaStorage = \Drupal::entityTypeManager()->getStorage('media');
    $mids = $mediaStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('bundle', 'document')
      ->condition('field_media_of', $node_id)
      ->range(0, 1)
      ->sort('created', 'DESC')
      ->execute();

    // Get media entities.
    if (!empty($mids)) {
      $media_id = reset($mids);
      if ($media_id) {
        // Load media and get actual file path.
        $media = Media::load($media_id);
        $form['node_entity'] = [
          '#type' => 'hidden',
          '#value' => $node_id,
        ];
        $form['media_entity'] = [
          '#type' => 'hidden',
          '#value' => $media_id,
        ];
        /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
        if (!$lock_service->locking($entity->id(), $entity->language()->getId(), 'edit-content', $user->id(), $entity_type)) {
          $entity_form_display = \Drupal::entityTypeManager()->getStorage('entity_form_display')->load('media.document.repository_xml_disable');
        }
        else {
          $entity_form_display = \Drupal::entityTypeManager()->getStorage('entity_form_display')->load('media.document.repository_xml');
        }
        $form['#parents'] = [];
        if ($widget = $entity_form_display->getRenderer('name')) {
          $items = $media->get('name');
          $items->filterEmptyItems();
          $form['name'] = $widget->form($items, $form, $form_state);
          $form['name']['#access'] = $items->access('edit');
        }

        if ($file_widget = $entity_form_display->getRenderer('field_media_document')) {
          $items = $media->get('field_media_document');
          $items->filterEmptyItems();
          $form['field_media_document'] = $file_widget->form($items, $form, $form_state);
          $form['field_media_document']['#access'] = $items->access('edit');
        }

        // We lock the content if it is currently edited by another user.
        if (!$lock_service->locking($entity->id(), $entity->language()->getId(), 'edit-content', $user->id(), $entity_type)) {
          $form['#disabled'] = TRUE;
          // Do not allow deletion, publishing, or unpublishing if locked.
          foreach (['delete', 'publish', 'unpublish'] as $key) {
            if (isset($form['actions'][$key])) {
              unset($form['actions'][$key]);
            }
          }
          // If moderation state is in use also disable corresponding buttons.
          if (isset($form['moderation_state'])) {
            unset($form['moderation_state']);
          }
        }
        else {
          // ContentLock::locking() returns TRUE if the content is locked by the
          // current user. Add an unlock button only for this user.
          $form['actions']['unlock'] = $lock_service->unlockButton($entity_type, $entity->id(), $entity->language()->getId(), 'edit-content', \Drupal::request()->query->get('destination'));
        }

        // Adding default media doc when loading edit page.
        if ($media->bundle() == 'document') {
          if (!empty($media->get('field_media_document')->getValue())) {
            $fid = $media->get('field_media_document')->getValue()[0]['target_id'];
            $file = File::load($fid);

            if (!empty($file)) {
              $ext = pathinfo($file->getFileUri(), PATHINFO_EXTENSION);

              if ($ext == 'xml' || $ext == 'json') {
                // Create a temp file to store original file value.
                $extension_list = \Drupal::service('extension.list.module');
                $filepath = $extension_list->getPath('leaf_workflow') . '/assets/temp_xml_file.xml';

                $directory = 'public://media-rev-temp';
                /** @var \Drupal\Core\File\FileSystemInterface $file_system */
                $file_system = \Drupal::service('file_system');
                $file_system->prepareDirectory($directory, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
                $file_system->copy($filepath, $directory . '/media_' . $media->id(), FileSystemInterface::EXISTS_REPLACE);

                // Copy original file data into temp file.
                $absolute_path = \Drupal::service('file_system')->realpath($file->getFileUri());

                if (!empty($absolute_path)) {
                  $data = file_get_contents($absolute_path);
                  $file = \Drupal::service('file.repository')->writeData($data, 'public://media-rev-temp/' . 'media_' . $media->id(), FileSystemInterface::EXISTS_REPLACE);
                }
              }
            }
          }
        }

        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Save'),
          '#button_type' => 'primary',
        ];
      }
    }
    else {
      $form['no_media'] = [
        '#markup' => $this->t('No Document found.'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_value = $form_state->getValues();
    $media_entity = Media::load($form_value['media_entity']);
    leaf_workflow_media_file_create_revision($media_entity, $form_state, TRUE);
  }

}
