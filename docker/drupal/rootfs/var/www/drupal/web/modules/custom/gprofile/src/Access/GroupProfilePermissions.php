<?php

namespace Drupal\gprofile\Access;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\profile\Entity\ProfileType;
use Drupal\profile\Entity\ProfileTypeInterface;

/**
 * Provides dynamic permissions for groups of different types.
 */
class GroupProfilePermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of group type permissions.
   *
   * @return array
   *   The group type permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function groupPermissions() {
    $perms = [];

    // Generate group permissions for all group types.
    foreach (ProfileType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }
    return $perms;
  }

  /**
   * Returns a list of group permissions for a given profile type.
   *
   * @param ProfileTypeInterface $type
   *   The profile type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(ProfileTypeInterface $type) {
    if(!empty($type)) {
      $type_id = $type->id();
      $type_name = $type->label();
      $type_params = ['@type_name' => $type->label()];

      return [
        "view $type_id member profiles" => [
          'title' => "View $type_name Profiles of Group Members",
          'title_args' => $type_params,
        ],
        "view unpublished $type_id member profiles" => [
          'title' => "View Unpublished $type_name  Profiles of Group Members",
          'title_args' => $type_params,
        ],
      ];
    }
  }

}
