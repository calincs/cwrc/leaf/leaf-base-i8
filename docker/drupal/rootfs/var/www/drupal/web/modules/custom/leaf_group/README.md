# Leaf Group

A bridge module between [leaf](https://gitlab.com/calincs/cwrc/leaf/leaf-base-i8) and the group module.

## 🎉 Features

### Configurations

- Adds two taxonomy vocabularies: Project categories and Sponsors
- Adds two group types: Personal Workspace and Project

On top of the four entity bundles configurations above, this module also adds initial configurations for fields, form mode,
entity form display, views, entity browser to support the entities bundles above.

### Codes

- A constraint plugin to ensure that a user can not have more than one personal workspace group: see
`src/Plugin/Validation/Constraint` and `leaf_group_entity_base_field_info_alter()` inside `leaf_group.module` for the
constraint usage.
- A form `src/Form/PersonalWorkspaceSettingsForm.php` to display:
  - a button to create a personal workspace when no workspace is available for the targeted user
  - or settings links when a user has an existing workspace
- A block plugin `src/Plugin/Block/PersonalWorkspaceSettings.php` to help with the placement of the form and allow the
user to interact with the personal workspace settings form.

## ‼️ Dependencies

### Existing

The dependencies below are the ones who already exists inside `<codebase>/composer.lock` of the leaf base.

- [group](https://drupal.org/project/group)
- [auto_entitylabel](https://drupal.org/project/auto_entitylabel)
- [field_permissions](https://drupal.org/project/field_permissions)
- [inline_entity_form](https://drupal.org/project/inline_entity_form)

### New

- [entity_browser:^2.6](https://drupal.org/project/entity_browser)
- [filefield_paths:1.x-dev](https://drupal.org/project/filefield_paths)
- [focal_point:^1.5](https://drupal.org/project/focal_point)

## 🛠 Installation

- Clone this module under `<codebase>/web/modules/custom`
- Delete `<codebase>/web/modules/custom/leaf_group/.git` folder. See reason under [notes](#-notes)
- Navigate back under `<codebase>`
- Download the "New" dependencies: `composer require drupal/entity_browser:^2.6 drupal/filefield_paths:1.x-dev drupal/focal_point:^1.5`
- Ensure none of the configurations in `config/install` are not already configured with the same ids
- Install the module using drush. From Drupal root folder run `./vendor/bin/drush en leaf_group -y`

## 📝 Notes

- For more information about the specs which lead to the creation of this module see:
  - [[Drupal] Create sponsor taxonomy](https://gitlab.com/calincs/cwrc/leaf/leaf-base-i8/-/issues/57)
  - [[Drupal] Group module - install and initial configuration](https://gitlab.com/calincs/cwrc/leaf/leaf-base-i8/-/issues/43)
- This module was implemented to work inside [leaf](https://gitlab.com/calincs/cwrc/leaf/leaf-base-i8) project.
- Once this module is part of the LEAF drupal codebase, further updates to it are suppose to happen there.
