<?php

namespace Drupal\leaf_workflow\Controller;

use Drupal\Core\Access\AccessResult;

/**
 * Builds an example page.
 */
class AccessController {

  /**
   * Checks access for a specific request.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access() {
    $current_path = \Drupal::service('path.current')->getPath();
    $path_explode = explode('/', $current_path);
    $nid = $path_explode[2] ?? '';
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);

    if (isset($node) && $path_explode[1] == 'node' && is_numeric($nid)) {
      if ($node->bundle() == 'islandora_object') {
        if (\Drupal::currentUser()->isAnonymous() && $node->get('moderation_state')->getString() != "published") {
          return AccessResult::forbidden();
        }
      }
    }

    return AccessResult::allowed();
  }

}
