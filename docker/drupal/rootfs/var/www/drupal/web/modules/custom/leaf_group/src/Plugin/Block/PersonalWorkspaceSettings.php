<?php

namespace Drupal\leaf_group\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\leaf_group\Form\PersonalWorkspaceSettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Workspace settings block'.
 *
 * @Block(
 *   id = "leaf_group_personal_workspace_settings",
 *   admin_label = @Translation("Leaf Group Personal Workspace settings"),
 *   category = @Translation("Leaf"),
 * )
 */
class PersonalWorkspaceSettings extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new PersonalWorkspaceSettings instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface  $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'user_context' => 'route',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['user_context'] = [
      '#type' => 'radios',
      '#title' => $this->t('User context'),
      '#required' => TRUE,
      '#options' => [
        'route' => $this->t('From Url'),
        'current_user' => $this->t('From the current logged in user'),
      ],
      '#default_value' => $this->configuration['user_context'],
      '#description' => $this->t('In which context the user for the personal workspace should be retrieved? If the url where you are placing this block contains the user id select "From Url" otherwise select the other option.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form_state = (new FormState())
      ->set('user_context', $this->configuration['user_context']);
    return $this->formBuilder->buildForm(PersonalWorkspaceSettingsForm::class, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    if ($account->isAnonymous()) {
      return $return_as_object ? AccessResult::forbidden('personal workspace are only available for logged in users.') : FALSE;
    }
    return parent::access($account, $return_as_object);
  }

}
