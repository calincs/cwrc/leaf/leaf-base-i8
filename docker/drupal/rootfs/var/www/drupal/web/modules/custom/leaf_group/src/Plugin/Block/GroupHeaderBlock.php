<?php

namespace Drupal\leaf_group\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'Group Header' Block.
 *
 * @Block(
 *   id = "group_header_block",
 *   admin_label = @Translation("Group Header Block (Title & Logo)"),
 *   category = @Translation("Group Header Block (Title & Logo)"),
 * )
 */
class GroupHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $route = \Drupal::routeMatch();
    $group = $route->getParameter('group');

    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
  
    if ($path_args[1] == "group" && (isset($path_args[2]) && is_numeric($path_args[2])) && (!empty($path_args[3]) && $path_args[3] == "search")) {
      $group = \Drupal::entityTypeManager()->getStorage('group')->load($path_args[2]);
    }

    if (!empty($group)) {

      if ($group->bundle() == 'project') {
        $logo_container = "";
        if (!empty($group->field_logo->entity)) {
          if (!empty($group->field_logo->entity->getFileUri())) {
            $image_url = \Drupal::service('file_url_generator')->generateAbsoluteString($group->field_logo->entity->getFileUri());
            $logo_container = "<div class='group-logo'><img src='" . $image_url . "' width='100%' height='auto'></div>";
          }
        }

        $group_markup = "<a href='/group/" . $group->id() . "'><div class='logo-label-wrapper'>" . $logo_container . "<div class='group-title block-page-title-block'><h1>" . leaf_baseline_trim_words($group->label(), 8) . "</h1></div></div></a>";
      }
    }

    return [
      '#markup' => !empty($group_markup) ? $group_markup : "",
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // With this when your node change your block will rebuild.
    if ($group = \Drupal::routeMatch()->getParameter('group')) {
      // If there is node add its cachetag.
      return Cache::mergeTags(parent::getCacheTags(), ['group:' . $group->id()]);
    }
    else {
      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // If you depends on \Drupal::routeMatch()
    // you must set context of this block with 'route' context tag.
    // Every new route this block will rebuild.
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

}
