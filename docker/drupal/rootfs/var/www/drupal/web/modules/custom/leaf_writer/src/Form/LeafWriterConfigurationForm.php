<?php

namespace Drupal\leaf_writer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LeafWriterConfigurationForm.
 *
 * @package Drupal\leafwriter\Form
 */
class LeafWriterConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'leafwriter.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'leafwriter_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('leafwriter.settings');
    $form['auth_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('LEAF Writer Authentication Token'),
      '#description' => $this->t('Add a token string which will be used in authentication for LEAF Writer.'),
      '#default_value' => $config->get('auth_token'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('leafwriter.settings')
      ->set('auth_token', $form_state->getValue('auth_token'))
      ->save();
  }

}
