<?php

namespace Drupal\leaf_group\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a group author has only one group personal workspace.
 *
 * @Constraint(
 *   id = "GroupPersonalWorkspaceOwner",
 *   label = @Translation("Group personal workspace owner", context = "Validation"),
 *   type = {"entity"},
 * )
 */
class GroupPersonalWorkspaceOwnerUnique extends Constraint {

  public $message = 'The user %name can only have one personal workspace.';

}
