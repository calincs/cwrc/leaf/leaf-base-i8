<?php

namespace Drupal\leaf_workflow\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'leaf_workflow' block.
 *
 * @Block(
 *   id = "leaf_workflow_block",
 *   admin_label = @Translation("Leaf workflow form block"),
 *   category = @Translation("Leaf workflow form block")
 * )
 */
class LeafWorkflowBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\leaf_workflow\Form\LeafWorkflow');
    return $form;
  }

}
