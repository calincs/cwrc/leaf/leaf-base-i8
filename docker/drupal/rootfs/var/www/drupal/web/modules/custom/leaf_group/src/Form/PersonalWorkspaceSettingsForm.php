<?php

namespace Drupal\leaf_group\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Personal Workspace settings form.
 */
class PersonalWorkspaceSettingsForm extends FormBase {

  protected $entityTypeManager;

  protected $currentUser;

  /**
   * Constructs the PersonalWorkspaceSettingsForm instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match) {
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
    $this->currentUser = $this->currentUser();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'leaf_group_personal_workspace_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $context = $form_state->get('user_context');
    $message = NULL;

    $form['#cache']['contexts'][] = 'user';
    $form['#cache']['contexts'][] = 'url.path';

    // Check the provided context.
    if (!$context || !in_array($context, ['current_user', 'route'])) {
      $message = $this->t('We couldn\'t determine in which context the user for the personal workspace should be retrieved.');
    }
    if (!($user = $this->getUser($context))) {
      $message = $this->t('We couldn\'t retrieve the user base on the provided context.');
    }
    // Displays the warning messages.
    if ($message) {
      $form['warning'] = [
        '#theme' => 'status_messages',
        '#message_list' => ['warning' => [
          $message,
          $this->t('Please contact the system administrator if this problem persist.'),
        ]],
        '#status_headings' => [
          'warning' => $this->t('Warning message'),
        ],
      ];
      return $form;
    }

    // Displays the workspace settings links
    if ($workspace = $this->getPersonalWorkspaceByUser($user)) {
      $route_names = [
        'entity.group.edit_form' => $this->t('Edit Workspace Description'),
        'view.group_members.page_1' => $this->t('Edit Workspace Membership'),
        'entity.group.delete_form' => $this->t('Delete Workspace'),
      ];
      $links = [];
      $gid = $workspace->id();
      foreach ($route_names as $route_name => $title) {
        $url = Url::fromRoute($route_name, ['group' => $gid]);
        if ($url->access($this->currentUser)) {
          $links[] = [
            '#type' => 'link',
            '#title' => $title,
            '#url' => $url,
          ];
        }
      }

      $form['items'] = [
        '#theme' => 'item_list',
        '#items' => $links,
        '#empty' => $this->t('You don\'t have access to any of the workspace settings links.'),
        '#cache' => [
          'tags' => ['group:' . $gid],
        ],
      ];
      return $form;
    }

    // In case no workspace.
    $is_admin = $this->currentUser->hasPermission('bypass group access');
    $is_workspace_owner = $this->currentUser->id() === $user->id();
    $access_handler = $this->entityTypeManager->getAccessControlHandler('group');
    $can_create_workspace = $access_handler->createAccess('personal_workspace', $this->currentUser);
    $can_create_own_workspace = $is_workspace_owner && $can_create_workspace;
    // Only admin or people viewing their own profile can create workspace.
    if ($can_create_own_workspace || $is_admin) {
      $form['info'] = [
        '#markup' => $can_create_own_workspace ?
          $this->t('You don\'t have a workspace configured yet.') :
          $this->t('@user doesn\'t have a workspace configured yet.', ['@user' => $user->getDisplayName()]),
      ];
      $form['create_workspace'] = [
        '#type' => 'submit',
        '#value' => $this->t('Create workspace'),
        '#workspace_owner' => $user,
        '#creating_own_workspace' => $can_create_own_workspace,
      ];
    }
    else {
      $form['info'] = [
        '#markup' => $this->t('<p>No workspace settings available for display.</p>'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = $this->messenger();
    $triggering_element = $form_state->getTriggeringElement();
    /** @var \Drupal\Core\Session\AccountInterface $workspace_owner */
    $workspace_owner = $triggering_element['#workspace_owner'];
    $creating_own_workspace = $triggering_element['#creating_own_workspace'];

    $group_storage = $this->entityTypeManager->getStorage('group');
    $workspace = $group_storage->create([
      'type' => 'personal_workspace',
      'uid' => $workspace_owner->id(),
    ]);
    try {
      $workspace->save();
      if ($creating_own_workspace) {
        $message = $this->t('You have successfully created your workspace. Please add a description if needed.');
      }
      else {
        $message = $this->t('You have successfully created @user workspace. Please add a description if needed.', [
          '@user' => $workspace_owner->getDisplayName(),
        ]);
      }
      $messenger->addStatus($message);
      $options = [
        'query' => $this->getDestinationArray(),
      ];
      $form_state->setRedirect('entity.group.edit_form', ['group' => $workspace->id()], $options);
    }
    catch (\Exception $e) {
      if ($creating_own_workspace) {
        $message = $this->t('An attempt to create your workspace failed. Please try again and if the problem persist contact the site administrator.');
      }
      else {
        $message = $this->t('An attempt to create @user workspace failed. Please try again and if the problem persist contact the site administrator.', [
          '@user' => $workspace_owner->getDisplayName(),
        ]);
      }
      $messenger->addError($message);
      watchdog_exception('leaf_group', $e);
    }
  }

  /**
   * Gets the user based on the context.
   *
   * @param string $context
   *   The context in which the user should be find in.
   *
   * @return \Drupal\Core\Session\AccountInterface|null
   *   The user to display the workspace.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getUser(string $context): ?AccountInterface {
    if ($context === 'current_user') {
      return $this->currentUser;
    }

    // Trying to get user from the current route parameter.
    $user = $this->getRouteMatch()->getParameter('user');
    if ($user instanceof AccountInterface) {
      return $user;
    }

    if (!is_integer($user)) {
      return NULL;
    }

    /** @var \Drupal\user\UserStorageInterface $user_storage */
    $user_storage = $this->entityTypeManager->getStorage('user');
    return $user_storage->load($user);
  }

  /**
   * Gets the personal workspace for the given user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user to retrieve the personal workspace for.
   *
   * @return \Drupal\group\Entity\GroupInterface|null
   *   The personal workspace group if available, NULL otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getPersonalWorkspaceByUser(AccountInterface $user): ?GroupInterface {
    /** @var \Drupal\group\Entity\Storage\GroupStorage $group_storage */
    $group_storage = $this->entityTypeManager->getStorage('group');
    // @todo check if we shouldn't turn off accessCheck.
    $gids = $group_storage->getQuery()->accessCheck(TRUE)
      ->condition('type', 'personal_workspace')
      ->condition('uid', $user->id())
      ->range(0, 1)
      ->execute();

    if (!$gids) {
      return NULL;
    }

    $gid = reset($gids);
    return $group_storage->load($gid);
  }

}
