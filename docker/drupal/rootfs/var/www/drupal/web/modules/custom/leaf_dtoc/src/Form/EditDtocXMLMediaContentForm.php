<?php

namespace Drupal\leaf_dtoc\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\leaf_dtoc\Controller\DtocJsonData;
use Drupal\media\Entity\Media;
use Drupal\node\NodeInterface;

/**
 * Class EditDtocXMLMediaContentForm.
 *
 * @package Drupal\leaf_dtoc\Form
 */
class EditDtocXMLMediaContentForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'leafdtoc_media_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm($form, FormStateInterface $form_state, $node = NULL) {

    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof NodeInterface) {
      $mediaStorage = \Drupal::entityTypeManager()->getStorage('media');
      $mids = $mediaStorage->getQuery()
        ->accessCheck(FALSE)
        ->condition('bundle', 'document')
        ->condition('field_media_of', $node->id())
        ->sort('created', 'DESC')
        ->execute();

      if (!empty($mids)) {
        $media_id = reset($mids);
        if ($media_id) {
          $mid = $media_id;

          $dtocController = new DtocJsonData();
          $media_content = $dtocController->getMediaDtocObject($mid);

          $form['#attached']['drupalSettings']['leafDToC']['json_data'] = $media_content;
        }
      }
    }

    // Library added to handle dtoc edit page default values and check/uncheck.
    $form['#attached']['library'][] = 'leaf_dtoc/edit-dtoc';

    if (!empty($media_content)) {
      $dtoc_media_stored_content = json_decode($media_content);
    }

    // Fieldset one.
    $form['documents'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Documents'),
      '#prefix' => '<div id="document-collections-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    // Define selected collection field value.
    $collection_field_selected_value = $form_state->get('collection_field_selected_value');
    if ($collection_field_selected_value === NULL) {
      $form_state->set('collection_field_selected_value', '');
      $collection_field_selected_value = '';
    }

    // Define selected collection field value - index.
    $removed_select_indexes = $form_state->get('removed_select_indexes');
    if ($removed_select_indexes === NULL) {
      $form_state->set('removed_select_indexes', []);
      $removed_select_indexes = $form_state->get('removed_select_indexes');
    }

    $search_title = t('search CWRC collections');
    $form['documents']['search_collections'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search CWRC collections'),
      '#description' => $this->t('If you want to add documents from different collections, add all collections here, comma-separated.'),
      '#autocomplete_route_name' => 'leaf_dtoc.autocomplete.repository',
      '#attributes' => [
        'placeholder' => $search_title,
      ],
      '#required' => TRUE,
    ];
    if (!empty($dtoc_media_stored_content->autoCompleteVal)) {
      $form['documents']['search_collections']['#default_value'] = $dtoc_media_stored_content->autoCompleteVal;
    }

    $form['documents']['actions']['select_files'] = [
      '#type' => 'submit',
      '#value' => $this->t('Show compatible documents'),
      '#submit' => [[$this, 'selectFileSubmit']],
      '#name' => 'fetch_file',
      '#ajax' => [
        'callback' => [$this, 'selectfileCallback'],
        'wrapper' => 'document-collections-fieldset-wrapper',
      ],
    ];

    if ($collection_field_selected_value != '' || !empty($dtoc_media_stored_content->autoCompleteVal)) {

      if (!empty($dtoc_media_stored_content->autoCompleteVal)) {
        $selected_autocomplete_text_array = explode(",", $dtoc_media_stored_content->autoCompleteVal);
      }

      if ($collection_field_selected_value != '') {
        $selected_autocomplete_text_array = explode(",", $collection_field_selected_value);
      }

      $media_file_urls = [];

      foreach ($selected_autocomplete_text_array as $selected_autocomplete_text) {
        preg_match('/(?<=\()(.+)(?=\))/is', $selected_autocomplete_text, $match);
        $selected_nid = end($match);

        if (in_array('remove_collection_button_' . $selected_nid, $removed_select_indexes)) {
          continue;
        }

        $media_file_urls[$selected_nid] = $this->getMediaFilesOfOneCollection($selected_nid);

        $media_file_urls_final = $media_file_urls[$selected_nid];

        $form['documents']['selected_collection_label' . $selected_nid] = [
          '#markup' => '<h6>' . $selected_autocomplete_text . '</h6>',
        ];

        $form['documents']['check_uncheck_all_' . $selected_nid] = [
          '#type' => 'checkboxes',
          '#title' => '',
          '#options' => ['Check / uncheck all'],
          '#attributes' => [
            'class' => ['selectall'],
          ],
        ];

        $form['documents']['select_files_' . $selected_nid] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Select Documents'),
          '#options' => $media_file_urls_final,
          '#prefix' => '<div id="collecion-xml-files-options">',
          '#suffix' => '</div>',
        ];
        if (!empty($dtoc_media_stored_content->input->inputs)) {
          $form['documents']['select_files_' . $selected_nid]['#default_value'] = $dtoc_media_stored_content->input->inputs;
        }

        $form['documents']['remove_collection_button_' . $selected_nid] = [
          '#type' => 'submit',
          '#value' => $this->t('Remove'),
          '#submit' => [[$this, 'removeCollectionSubmit']],
          '#name' => 'remove_collection_button_' . $selected_nid,
          '#ajax' => [
            'callback' => [$this, 'removeCollectionCallback'],
            'wrapper' => 'document-collections-fieldset-wrapper',
          ],
        ];
      }
    }

    // Fieldset two.
    $form['corpus_parts_index_config'] = [
      '#type' => 'fieldset',
      '#title' => t('Corpus parts and index configuration'),
      '#collapsible' => TRUE,
    ];
    $form['corpus_parts_index_config']['simplify_config'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Simplify configuration etc.'),
      '#description' => 'Simplify configuration etc(`ignoreNamespace`)',
    ];
    $form['corpus_parts_index_config']['part'] = [
      '#type' => 'textfield',
      '#title' => t('Part'),
      '#description' => 'Part(`documents`)',
      '#default_value' => "//front|//div[@type='chapter']|//div[@type='preface']|//div[@type='afterword']|//div[@type='contributors']",
      '#required' => TRUE,
    ];
    $form['corpus_parts_index_config']['part_content'] = [
      '#type' => 'textfield',
      '#title' => t('Part Content'),
      '#description' => 'Part(`documentContent`)',
    ];
    $form['corpus_parts_index_config']['title'] = [
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#description' => 'Title(`documentTitle`)',
      '#default_value' => "//head/title",
      '#required' => TRUE,
    ];
    $form['corpus_parts_index_config']['part_author'] = [
      '#type' => 'textfield',
      '#title' => t('Part Author'),
      '#description' => 'Title(`documentAuthor`)',
    ];
    $form['corpus_parts_index_config']['index'] = [
      '#type' => 'textfield',
      '#title' => t('Index'),
      '#description' => 'Index(`indexDocument`)',
    ];

    // Fieldset three.
    $form['formatting'] = [
      '#type' => 'fieldset',
      '#title' => t('Formatting'),
      '#collapsible' => TRUE,
    ];
    $form['formatting']['figures'] = [
      '#type' => 'textfield',
      '#title' => t('Figures'),
      '#description' => 'Figures(`documentImages`)',
    ];
    $form['formatting']['notes'] = [
      '#type' => 'textfield',
      '#title' => t('Notes'),
      '#description' => 'Notes(`documentNotes`)',
    ];
    $form['formatting']['references'] = [
      '#type' => 'textfield',
      '#title' => t('References'),
      '#description' => 'References(`documentLinks`)',
    ];

    // Fieldset four.
    $form['markup_curation_parts_ordering'] = [
      '#type' => 'fieldset',
      '#title' => t('Formatting'),
      '#collapsible' => TRUE,
    ];
    $form['markup_curation_parts_ordering']['markup_index'] = [
      '#title' => t('Markup'),
      '#type' => 'textarea',
      '#description' => 'Markup index(`curation.markup`)',
      '#rows' => 5,
      '#cols' => 20,
      '#resizable' => TRUE,
    ];
    $form['markup_curation_parts_ordering']['toc'] = [
      '#title' => t('Table of Contents'),
      '#type' => 'hidden',
      '#description' => 'Markup index(`curation.toc`)',
      '#rows' => 5,
      '#cols' => 20,
      '#resizable' => TRUE,
    ];

    $form['dtoc_revision_log_message'] = [
      '#title' => t('Revision log message'),
      '#type' => 'textarea',
      '#description' => t('Briefly describe the changes you have made.'),
      '#rows' => 5,
      '#cols' => 20,
      '#resizable' => TRUE,
      '#required' => FALSE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#name' => 'final_submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Callback for select file ajax-enabled buttons.
   */
  public function removeCollectionCallback(array &$form, FormStateInterface $form_state) {
    return $form['documents'];
  }

  /**
   * Select file submit handler.
   */
  public function removeCollectionSubmit(array &$form, FormStateInterface $form_state) {
    ;
    $trigger = $form_state->getTriggeringElement();
    $indexToRemove = $trigger['#name'];

    // Keep track of removed fields so we can add new fields at the bottom
    // Without this they would be added where a value was removed.
    $removed_select_indexes = $form_state->get('removed_select_indexes');
    $removed_select_indexes[] = $indexToRemove;
    $form_state->set('removed_select_indexes', $removed_select_indexes);

    $form_state->setRebuild();
  }

  /**
   * Callback for select file ajax-enabled buttons.
   */
  public function selectfileCallback(array &$form, FormStateInterface $form_state) {
    return $form['documents'];
  }

  /**
   * Select file submit handler.
   */
  public function selectFileSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->set('collection_field_selected_value', $form_state->getValue('search_collections'));
    $form_state->set('removed_select_indexes', []);

    $form_state->setRebuild();
  }

  /**
   *
   */
  public function getAllXmlFiles($auto_selected_value = '') {
    $media_file_url = [];

    if (empty($auto_selected_value)) {
      return $media_file_url;
    }

    $selected_autocomplete_text_array = explode(",", $auto_selected_value);

    foreach ($selected_autocomplete_text_array as $selected_autocomplete_text) {
      preg_match('/(?<=\()(.+)(?=\))/is', $selected_autocomplete_text, $match);
      $selected_nid = end($match);
      $media_file_urls[] = $this->getMediaFilesOfOneCollection($selected_nid);
    }

    return $media_file_url;
  }

  /**
   *
   */
  public function getMediaFilesOfOneCollection($collection_id = '') {

    $media_file_urls = [];

    if (!empty($collection_id)) {
      // Get the child member repo items.
      $child_group_nodes = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadByProperties([
          'field_member_of' => $collection_id,
          'type' => 'islandora_object',
        ]);

      foreach ($child_group_nodes as $child_node) {
        $model_terms = $child_node->get('field_model')->referencedEntities();
        foreach ($model_terms as $term) {
          $model_name = $term->getName();
        }

        $resource_terms = $child_node->get('field_resource_type')->referencedEntities();
        foreach ($resource_terms as $term) {
          $resource_name = $term->getName();
        }
        if ($model_name == 'Digital Document' && $resource_name == 'Text') {
          $mediaStorage = \Drupal::entityTypeManager()->getStorage('media');
          $mids = $mediaStorage->getQuery()
            ->accessCheck(FALSE)
            ->condition('bundle', 'document')
            ->condition('field_media_of', $child_node->id())
            ->sort('created', 'DESC')
            ->execute();

          if (!empty($mids) && count($mids) >= 1) {
            foreach ($mids as $media_id) {
              $media = Media::load($media_id);
              $file_uri = $media->field_media_document->entity->getFileUri();
              $ext = pathinfo($file_uri, PATHINFO_EXTENSION);
              if ($ext == 'xml') {
                $media_file_urls[\Drupal::service('file_url_generator')->generateAbsoluteString($file_uri)] = $child_node->getTitle();
              }
            }
          }
        }
      }
    }

    return $media_file_urls;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    global $base_url;

    $auto_selected_value = $form_state->getValue('search_collections');
    $removed_select_indexes = $form_state->get('removed_select_indexes');

    $mediaStorage = \Drupal::entityTypeManager()->getStorage('media');

    // Get selected autocomplete node media file url.
    if (!empty($auto_selected_value)) {

      $selected_auto_text_array = explode(",", $auto_selected_value);
      $selected_nids = [];
      foreach ($selected_auto_text_array as $selected_autocomplete_text) {
        if (in_array($selected_autocomplete_text, $removed_select_indexes)) {
          continue;
        }
        preg_match('/(?<=\()(.+)(?=\))/is', $selected_autocomplete_text, $match);
        $selected_nids[] = end($match);
      }

      $selected_xml_files = [];

      if (!empty($selected_nids)) {
        foreach ($selected_nids as $sel_nid) {
          $select_xml_files = $form_state->getValue('select_files_' . $sel_nid);
          if (!empty($select_xml_files)) {
            foreach ($select_xml_files as $value) {
              if ($value == '0') {
                continue;
              }
              $selected_xml_files[] = $value;
            }
          }
        }
      }

      if (!empty($selected_xml_files)) {
        $media_file_url = $selected_xml_files;
      }
      else {
        $selected_autocomplete_text_array = explode(",", $auto_selected_value);
        $media_file_url = [];

        foreach ($selected_autocomplete_text_array as $selected_autocomplete_text) {
          if (in_array($selected_autocomplete_text, $removed_select_indexes)) {
            continue;
          }
          preg_match('/(?<=\()(.+)(?=\))/is', $selected_autocomplete_text, $match);
          $selected_nid = end($match);

          // Get the child member repo items.
          $child_group_nodes = \Drupal::entityTypeManager()
            ->getStorage('node')
            ->loadByProperties([
              'field_member_of' => $selected_nid,
              'type' => 'islandora_object',
            ]);

          foreach ($child_group_nodes as $child_node) {
            $model_terms = $child_node->get('field_model')->referencedEntities();
            foreach ($model_terms as $term) {
              $model_name = $term->getName();
            }

            $resource_terms = $child_node->get('field_resource_type')->referencedEntities();
            foreach ($resource_terms as $term) {
              $resource_name = $term->getName();
            }
            if ($model_name == 'Digital Document' && $resource_name == 'Text') {
              $mids = $mediaStorage->getQuery()
                ->accessCheck(FALSE)
                ->condition('bundle', 'document')
                ->condition('field_media_of', $child_node->id())
                ->sort('created', 'DESC')
                ->execute();

              if (!empty($mids) && count($mids) >= 1) {
                foreach ($mids as $media_id) {
                  $media = Media::load($media_id);
                  $file_uri = $media->field_media_document->entity->getFileUri();
                  $ext = pathinfo($file_uri, PATHINFO_EXTENSION);
                  if ($ext == 'xml') {
                    $media_file_url[] = \Drupal::service('file_url_generator')->generateAbsoluteString($file_uri);
                  }
                }
              }
            }
          }
        }
      }

      if (!empty($media_file_url)) {
        $document_string = $form_state->getValue('part');
        $document_title = $form_state->getValue('title');
        $document_author = $form_state->getValue('part_author');
        $document_images = $form_state->getValue('figures');
        $document_notes = $form_state->getValue('notes');
        $document_links = $form_state->getValue('references');
        $indexed_ducument = $form_state->getValue('index');
        $document_content = $form_state->getValue('part_content');
        $document_markup_index = $form_state->getValue('markup_index');
        $document_toc = $form_state->getValue('toc');
        $document_selected_val = $form_state->getValue('search_collections');

        $dtoc_array = [
          "dtocRootUrl" => "/libraries/leafdtoc/public/",
          "input" => [
            "ignoreNamespace" => TRUE,
            "documents" => $document_string,
            "documentContent" => !empty($document_content) ? $document_content : '',
            "documentTitle" => $document_title,
            "documentAuthor" => $document_author,
            "documentImages" => $document_images,
            "documentNotes" => $document_notes,
            "documentLinks" => $document_links,
            "indexDocument" => $indexed_ducument,
            "inputs" => $media_file_url,
          ],
          'autoCompleteVal' => $document_selected_val,
        ];

        if (!empty($document_markup_index)) {
          $dtoc_array["input"]["curation"]["markup"] = json_decode($document_markup_index, TRUE);
        }

        if (!empty($document_toc)) {
          $dtoc_array["input"]["curation"]["toc"] = json_decode($document_toc, TRUE);
        }

        $json = Json::encode($dtoc_array);
        $this->setDestination();

        if (file_put_contents($this->getDestination(), $json) === FALSE) {
          // Write to output file failed - log in logger and in ResponseText on
          // batch execution page user will end up on if write to file fails.
          $message = t('Could not write to temporary output file for result export (@file). Check permissions.', ['@file' => $this->getDestination()]);
          \Drupal::logger('leaf_dtoc')->error($message);
        }
        else {
          $file_repository = \Drupal::service('file.repository');
          $json_file = $file_repository->writeData($json, $this->getDestination(), FileSystemInterface::EXISTS_REPLACE);

          $nid = '';
          $node = \Drupal::routeMatch()->getParameter('node');
          if ($node instanceof NodeInterface) {
            // You can get nid and anything else you need from the node object.
            $nid = $node->id();

            $mids = $mediaStorage->getQuery()
              ->accessCheck(FALSE)
              ->condition('bundle', 'document')
              ->condition('field_media_of', $nid)
              ->sort('created', 'DESC')
              ->execute();

            if (!empty($mids)) {
              $media_id = reset($mids);
              // In a function.
              $media = Media::load($media_id);
              $media->field_media_document->target_id = $json_file->id();
              $media->field_media_use->target_id = '18';
              $media->revision_log_message->value = !empty($form_state->getValue('dtoc_revision_log_message')) ? $form_state->getValue('dtoc_revision_log_message') : "No log message added.";
              // Do stuff with $media.
              $media->save();
            }
            else {
              $media = Media::create([
                'bundle' => 'document',
                'uid' => \Drupal::currentUser()->id(),
                'field_media_document' => [
                  'target_id' => $json_file->id(),
                ],
                'field_media_of' => [
                  'target_id' => $nid,
                ],
                'field_media_use' => [
                  'target_id' => '18',
                ],
                'revision_log_message' => [
                  'value' => !empty($form_state->getValue('dtoc_revision_log_message')) ? $form_state->getValue('dtoc_revision_log_message') : "No log message added.",
                ],
              ]);

              $media->setName('Sample dynamic table context Voyant - ' . $nid)
                ->setPublished(TRUE)
                ->save();
            }
          }

          \Drupal::messenger()->addMessage($this->t('DToC datastream has been updated'), 'status', TRUE);
          $form_state->setRedirect('view.manage_about.page_1', ['node' => $nid]);
        }
      }
    }
  }

  /**
   *
   */
  public function setDestination() {
    $fileSystem = \Drupal::service('file_system');

    $destination = $this->getDestination();
    $directory = $this->getDirectory();

    $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $uri = $fileSystem->saveData('', $destination, FileSystemInterface::EXISTS_REPLACE);

    if (!$uri) {
      \Drupal::logger('leaf_dtoc')->error(t('dtoc json data export file not careated'));
      return FALSE;
    }

    @chmod($destination, 0775);
  }

  /**
   *
   */
  private function getDirectory() {
    $directory = 'public://leaf_dtoc/';
    return $directory;
  }

  /**
   *
   */
  private function getDestination() {
    $date_string = date('Ymd', \Drupal::time()->getRequestTime());
    $nid = '';

    $directory = $this->getDirectory();
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof NodeInterface) {
      // You can get nid and anything else you need from the node object.
      $nid = $node->id();
    }

    $filename = 'dtoc_body_' . $nid . '.json';
    return $directory . $filename;
  }

}
