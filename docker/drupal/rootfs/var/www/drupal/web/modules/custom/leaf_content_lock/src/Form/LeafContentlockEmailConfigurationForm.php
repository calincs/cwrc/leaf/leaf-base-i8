<?php

/**
 * @file
 * Contains Drupal\leaf_content_lock\Form\LeafContentlockEmailConfigurationForm.
 */

namespace Drupal\leaf_content_lock\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LeafContentlockEmailConfigurationForm.
 *
 * @package Drupal\leaf_content_lock\Form
 */
class LeafContentlockEmailConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'leaf_content_lock.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'leaf_content_lock_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('leaf_content_lock.settings');
    $form['leaf_content_lock_email'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('LEAF Content Lock Email - Node Author'),
      '#description' => $this->t('Email text which is sent to node author when a lock break request is submitted.'),
      '#default_value' => $config->get('leaf_content_lock_email'),
    );

    $form['leaf_content_lock_requester_email'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('LEAF Content Lock Email - Requester'),
      '#description' => $this->t('Email text which is sent to lock break requester when admin breaks a lock.'),
      '#default_value' => $config->get('leaf_content_lock_requester_email'),
    );

    $form['tokens'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['site', 'node', 'customtoken'],
      '#global_types' => TRUE,
      '#click_insert' => TRUE,
      '#show_restricted' => FALSE,
      '#recursion_limit' => 3,
      '#text' => $this->t('Browse available tokens'),
    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('leaf_content_lock.settings')
      ->set('leaf_content_lock_email', $form_state->getValue('leaf_content_lock_email'))
      ->set('leaf_content_lock_requester_email', $form_state->getValue('leaf_content_lock_requester_email'))
      ->save();
  }

}
