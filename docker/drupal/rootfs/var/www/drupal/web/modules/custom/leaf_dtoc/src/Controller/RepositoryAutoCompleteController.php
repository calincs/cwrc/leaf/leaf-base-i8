<?php

namespace Drupal\leaf_dtoc\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class RepositoryAutoCompleteController extends ControllerBase {

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorage
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);

    $query = $this->nodeStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'islandora_object')
      ->condition('title', $input, 'CONTAINS')
      ->groupBy('nid')
      ->sort('created', 'DESC');

    $ids = $query->execute();
    $nodes = $ids ? Node::loadMultiple($ids) : [];
    foreach ($nodes as $node) {
      // Model and resource type should be collection.
      $model_terms = $node->get('field_model')->referencedEntities();
      foreach ($model_terms as $term) {
        $model_name = $term->getName();
      }
      $resource_terms = $node->get('field_resource_type')->referencedEntities();
      foreach ($resource_terms as $term) {
        $resource_name = $term->getName();
      }

      if ($model_name == "Collection" && $resource_name == "Collection") {
        $is_media_xml_available = $this->check_availability_media_xml_items_collection($node->id());
        if ($is_media_xml_available) {
          $results[] = [
            'value' => EntityAutocomplete::getEntityLabels([$node]),
            'label' => $node->getTitle() . ' (' . $node->id() . ')',
          ];
        }
      }
    }

    return new JsonResponse($results);
  }

  /**
   * Check media xml items exist inside collection.
   */
  public function check_availability_media_xml_items_collection($nid) {
    // Get the child member repo items.
    $child_group_nodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties([
        'field_member_of' => $nid,
        'type' => 'islandora_object',
      ]);

    foreach ($child_group_nodes as $child_node) {
      $model_terms = $child_node->get('field_model')->referencedEntities();
      foreach ($model_terms as $term) {
        $model_name = $term->getName();
      }

      $resource_terms = $child_node->get('field_resource_type')->referencedEntities();
      foreach ($resource_terms as $term) {
        $resource_name = $term->getName();
      }

      $media_file_url = '';
      if ($model_name == 'Digital Document' && $resource_name == 'Text') {
        $mediaStorage = \Drupal::entityTypeManager()->getStorage('media');
        $mids = $mediaStorage->getQuery()
          ->accessCheck(FALSE)
          ->condition('bundle', 'document')
          ->condition('field_media_of', $child_node->id())
          ->sort('created', 'DESC')
          ->execute();

        if (!empty($mids) && count($mids) >= 1) {
          foreach ($mids as $media_id) {
            $media = Media::load($media_id);
            $file_uri = $media->field_media_document->entity->getFileUri();
            $media_file_url = \Drupal::service('file_url_generator')->generateAbsoluteString($file_uri);
            $ext = pathinfo($media_file_url, PATHINFO_EXTENSION);
            if ($ext == 'xml') {
              return TRUE;
            }
          }
        }
      }
    }

    return FALSE;
  }

}
