<?php

namespace Drupal\leaf_repository\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\File\FileSystemInterface;
use Drupal\media\Entity\Media;
use Drupal\Core\Url;


class LeafRepositoryDefaultMediaThumbnail extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate Default Thumbnails for Repository Items'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $default_images = [
      'audio-object.png',
      'binary-object.png',
      'book-parent-of-ages.png',
      'collection-object.png',
      'compound-object.png',
      'fallback-object-icon.png',
      'image-object.png',
      'large-image.png',
      'newspaper-issue.png',
      'newspaper-parent-of-issue.png',
      'remote-media.png',
      'video-object.png',
      'xml-document.png',
      'dtoc.png',
      'bibliographic-record.png'
    ];

    $module_path = \Drupal::service('extension.list.module')->getPath('leaf_repository') . "/images/";
    foreach ($default_images as $key => $value) {
      $media = \Drupal::entityTypeManager()
        ->getStorage('media')
        ->loadByProperties(['name' => $value]);

      if (empty($media) && !empty($value)) {
        $file_path = $module_path . $value;
        if (!empty($file_path)) {
          $file_data = file_get_contents($file_path);
          if ($file_data) {
            $file = \Drupal::service('file.repository')->writeData($file_data, 'public://' . $value, FileSystemInterface::EXISTS_REPLACE);
            if (!empty($file)) {
              // Use the entity type manager (recommended).
              $media = \Drupal::entityTypeManager()->getStorage('media')->create([
                'bundle' => 'image',
                'uid'=> \Drupal::currentUser()->id(),
                'field_media_image' => [
                  'target_id' => $file->id(),
                  'alt' => $file->getFileName(),
                ],
              ]);
              $media->setName($value)->setPublished(TRUE)->save();
            }
          }
        }
      }
    }
    \Drupal::messenger()->addMessage('Default thumbnails for repository items are created.');
  }
}