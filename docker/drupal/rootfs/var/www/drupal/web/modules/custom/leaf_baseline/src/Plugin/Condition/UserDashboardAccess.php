<?php

namespace Drupal\leaf_baseline\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'User profile Dashboard Access' condition.
 *
 * @Condition(
 *   id = "user_dashboard_access",
 *   label = @Translation("User Dashboard Page"),
 *   context_definitions = {
 *     "user" = @ContextDefinition("entity:user", label = @Translation("User")),
 *   }
 * )
 */
class UserDashboardAccess extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Service current_route_match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  private $currentRouteMatch;

  /**
   * Service entity_field.manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  private $entityFieldManager;

  /**
   * Service current_user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $currentUser;

  /**
   * UserProfilePage constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrentRouteMatch $currentRouteMatch, EntityFieldManager $entityFieldManager, AccountProxyInterface $currentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $currentRouteMatch;
    $this->entityFieldManager = $entityFieldManager;
    $this->currentUser = $currentUser;
  }

  /**
   * UserProfilePage create function.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_field.manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return t('User profile dashboard page.');
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $route = $this->currentRouteMatch->getCurrentRouteMatch();

    // Match all entity.user.* routes having user parameter,
    // which include regular user profile view (entity.user.canonical),
    // user edit form (entity.user.edit_form),...
    if (strpos($route->getRouteName(), 'page_manager.page_view_dashboard_dashboard-') === 0) {
      $user_id = $this->currentRouteMatch->getRawParameter('user');
      // Array List of role IDs.
      $current_user_roles = $this->currentUser->getRoles();

      if ($user_id === NULL || $this->currentUser->isAnonymous()) {
        return FALSE;
      }

      // Make all route accessible for admin.
      if (in_array('administrator', $current_user_roles)) {
        return TRUE;
      }
      elseif ($this->currentUser->id() == $user_id) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }

    return TRUE;
  }

}
