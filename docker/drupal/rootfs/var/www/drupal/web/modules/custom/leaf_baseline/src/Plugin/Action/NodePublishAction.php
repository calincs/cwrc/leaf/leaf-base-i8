<?php

namespace Drupal\leaf_baseline\Plugin\Action;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;

/**
 * Publish node action.
 *
 * @Action(
 *   id = "custom_node_publish_action",
 *   label = @Translation("Make Public"),
 *   type = "node"
 * )
 */
class NodePublishAction extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if ($entity->getEntityTypeId() == "node") {
      $languageCode = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $storage = \Drupal::entityTypeManager()->getStorage('node');
      $revision_id = $storage->getLatestTranslationAffectedRevisionId($entity->id(), $languageCode);

      if (!empty($revision_id)) {
        $revision = $storage->loadRevision($revision_id);
        $revision = $revision->getTranslation($languageCode);
        $revision->setRevisionCreationTime(\Drupal::time()->getCurrentTime());
        $revision = $storage->createRevision($revision);

        // Publish node through workflow.
        if ($entity->bundle() == 'islandora_object') {
          $revision->moderation_state = 'published';
          $revision->revision_log = "";
          $revision->save();
        }
        else {
          $revision->status = TRUE;
          $revision->revision_log = "";
          $revision->save();
        }

        return $this->t($entity->getTitle() . " - content is published.");
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\node\NodeInterface $object */
    $result = $object->access('update', $account, TRUE);

    return $return_as_object ? $result : $result->isAllowed();
  }

}
