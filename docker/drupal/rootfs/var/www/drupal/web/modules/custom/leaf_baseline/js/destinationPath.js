/**
 * @file
 * Adds the destination path to the login link.
 */

(function (Drupal, drupalSettings, once) {
    const settings = drupalSettings.leafbaseline.destinationPath;
    const excludedPathNames = settings.excludedPathNames;
    const windowLocation = window.location;
    let destinationPath = '';
  
    // Set the current path as the destination path. Making sure that we exclude
    // paths with the destination parameter or any path added to the excluded
    // path names settings.
    if (
      !/destination=/.test(windowLocation.search) &&
      !excludedPathNames.includes(windowLocation.pathname)
    ) {
      destinationPath = windowLocation.pathname.replace(/^\//, '');
    }
  
    if (!destinationPath && settings.siteFrontPage) {
      // Redirect to the site front page when the destination is not found.
      destinationPath = settings.siteFrontPage;
    }
  
    /**
     * Adds the current path as the destination path to the user login url.
     *
     * @type {Drupal~behavior}
     *
     * @prop {Drupal~behaviorAttach} attach
     *   Attaches the destination url query parameter to the login link element.
     */
    Drupal.behaviors.leafbaselineDestinationPath = {
      attach() {
        if (destinationPath !== '') {
          once('login-link', 'a[data-drupal-link-system-path="user/login"]').forEach((link) => {
            link.setAttribute('href', `/user/login?destination=${destinationPath}`);
          });
        }
      },
    };
  })(Drupal, drupalSettings, once);
  