<?php

namespace Drupal\leaf_baseline\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\profile\Entity\Profile;

/**
 * Provides a 'Group Member Page Header  (Group Title & First/Last Name)' Block.
 *
 * @Block(
 *   id = "group_member_page_header_block",
 *   admin_label = @Translation("Group Member Page Header Block"),
 *   category = @Translation("Group Member Page Header Block"),
 * )
 */
class GroupMemberPageHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $group_markup = "";

    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $member_id_url_arg = "";
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    // Get member id from url.
    if (!empty($path_args)) {
      if ((!empty($path_args[1]) && $path_args[1] == "group") && (!empty($path_args[2]) && is_numeric($path_args[2])) && (!empty($path_args[3]) && $path_args[3] == "content") && (!empty($path_args[4]) && is_numeric($path_args[4]))) {
        $member_id_url_arg = $path_args[4];
      }
    }

    $group = \Drupal::routeMatch()->getParameter('group');

    if (!empty($group)) {
      $gid = $group->id();
      $current_group = \Drupal::entityTypeManager()->getStorage('group')->load($gid);
      $current_group_title = $current_group->label();
      $group_markup .= "<a href='/group/" . $gid . "'><div class='group-title block-page-title-block'><h1>" . $current_group_title;

      // Get group members.
      $members = $current_group->getMembers();

      if (!empty($members) && !empty($member_id_url_arg)) {
        foreach ($members as $member) {
          $member_object = $member->getGroupContent();
          $member_id = $member_object->id();

          if ($member_id != $member_id_url_arg) {
            continue;
          }

          $user = $member->getUser();

          if ($user->hasTranslation($langcode)) {
            $user = $user->getTranslation($langcode);
          }

          $first_name = $user->field_first_name->value;
          $last_name = $user->field_last_name->value;

          $group_markup .= ": " . $first_name . " $last_name</h1></div></a>";
        }
      }
    }

    return [
      '#markup' => $group_markup,
    ];
  }

  /**
   * Cache max agae 0.
   *
   * @return int
   *   Returning max age.
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
