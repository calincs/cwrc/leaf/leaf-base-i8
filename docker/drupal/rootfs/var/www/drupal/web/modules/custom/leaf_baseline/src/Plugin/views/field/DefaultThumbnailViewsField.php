<?php

namespace Drupal\leaf_baseline\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\TermInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("default_thumbnail_field")
 */
class DefaultThumbnailViewsField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $node = NULL;

    if ((isset($values->_object) && $values->_object->getEntity()->getEntityTypeId() == "node") || (isset($values->_entity) && $values->_entity->getEntityTypeId() == "node")) {
      $node = $values->_entity;
    } else {
      // Get relationship content id.
      $nid = $values->_relationship_objects["entity:group_content/entity_id"][0]->target_id;
      if ($nid != NULL) {
        $node = Node::load($nid);
      }
    }

    if ($node == NULL) return;

    $media_thumbnail_uri = '';
    $mediaStorage = \Drupal::entityTypeManager()->getStorage('media');
    $mids = $mediaStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('bundle', 'image')
      ->condition('field_media_of', $node->id())
      ->range(0, 1)
      ->sort('created', 'DESC')
      ->execute();
    // Get media entities.
    if (!empty($mids)) {
      $media_id = reset($mids);
      if ($media_id) {
        // Load media and get actual file path.
        $media = Media::load($media_id);
        $fid = $media->field_media_image->target_id;
        if ($fid) {
          $file = File::load($fid);
          $media_thumbnail_uri = $file->getFileUri();
          if (\Drupal::service('file_url_generator')->generateAbsoluteString($media_thumbnail_uri) != '') {
            $thumbnail_url = \Drupal::service('file_url_generator')->generateAbsoluteString($media_thumbnail_uri);
          }
        }
      }
    }

    // Pull default thumbnails.
    if ($media_thumbnail_uri == '') {
      if (isset($node)) {
        $thumbnail_url = $this->getDefaultThumbnail($node);
      }
    }

    return $thumbnail_url;
  }

  /**
   * Get default thumbnail.
   */
  public function getDefaultThumbnail($entity) {
    $entity_type = $entity->getEntityTypeId();
    if ($entity_type == 'group_content') {
      $entity = $entity->getEntity();
    }
    $field_model_value = $field_resource_type_value = $field_display_hint_value = '';
    if ($entity->hasField('field_resource_type') && !$entity->get('field_resource_type')->isEmpty()) {
      $resource_type = $entity->field_resource_type->entity;
      if ($resource_type instanceof TermInterface) {
        $field_resource_type_value = $entity->get('field_resource_type')->entity->getName();
      }
    }
    if ($entity->hasField('field_model') && !$entity->get('field_model')->isEmpty()) {
      $mode_type = $entity->field_model->entity;
      if ($mode_type instanceof TermInterface) {
        $field_model_value = $entity->get('field_model')->entity->getName();
      }
    }

    if ($entity->hasField('field_display_hints') && !$entity->get('field_display_hints')->isEmpty()) {
      $display_hint = $entity->field_display_hints->entity;
      if ($display_hint instanceof TermInterface) {
        $field_display_hint_value = $entity->get('field_display_hints')->entity->getName();
      }
    }
    
    if ($field_resource_type_value == 'Text' && $field_model_value == 'Digital Document') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/xml-document.png';

      switch ($field_display_hint_value) {
        case "DToC":
          $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/dtoc.png';
          break;

        case "PDFjs":
          $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/pdf-object.png';
          break;
      }
    }
    elseif ($field_resource_type_value == 'Collection' && $field_model_value == 'Collection') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/collection-object.png';
    }
    elseif ($field_resource_type_value == 'Sound' && $field_model_value == 'Audio') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/audio-object.png';
    }
    elseif ($field_resource_type_value == 'Still Image' && $field_model_value == 'Image') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/image-object.png';
    }
    elseif ($field_resource_type_value == 'any' && $field_model_value == 'Binary') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/binary-object.png';
    }
    elseif ($field_resource_type_value == 'Collection' && $field_model_value == 'Paged Content') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/book-parent-of-ages.png';
    }
    elseif ($field_resource_type_value == 'Collection' && $field_model_value == 'Compound Object') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/compound-object.png';
    }
    elseif ($field_resource_type_value == 'Still Image' && $field_model_value == 'Image' && $field_display_hint_value == 'Open Seadragon') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/large-image.png';
    }
    elseif ($field_resource_type_value == 'Collection' && $field_model_value == 'Newspaper') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/newspaper-parent-of-issue.png';
    }
    elseif ($field_resource_type_value == 'Collection' && $field_model_value == 'Publication Issue') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/newspaper-issue.png';
    }
    elseif ($field_resource_type_value == 'Moving Image' && $field_model_value == 'Video') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/video-object.png';
    }
    elseif ($field_resource_type_value == 'Moving Image' && $field_model_value == 'Audio') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/remote-media.png';
    }
    elseif (in_array($field_resource_type_value, ['Moving Image', 'Sound']) && in_array($field_model_value, ['Video', 'Audio']) && $field_display_hint_value == 'Remote Media') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/remote-media.png';
    }
    elseif ($field_resource_type_value == 'Sound' && in_array($field_model_value, ['Video', 'Audio'])) {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/remote-media.png';
    }
    elseif ($field_resource_type_value == 'Text' && $field_model_value == 'Bibliographic Record') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/Bibliographic-Record.png';
    }
    elseif ($field_resource_type_value == 'Text' && $field_model_value == 'Citation') {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/Bibliographic-Record.png';
    }
    else {
      $thumbnail_path = '/themes/custom/leaf_base/images/repository_item/fallback-object-icon.png';
    }
    
    return $thumbnail_path;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

}
