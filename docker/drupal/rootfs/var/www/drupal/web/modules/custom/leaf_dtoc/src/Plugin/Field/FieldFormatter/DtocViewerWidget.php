<?php

namespace Drupal\leaf_dtoc\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Plugin implementation of the
 *    'DtocViewerWidget' formatter.
 *
 * @FieldFormatter(
 *   id = "dtoc_viewer",
 *   label = @Translation("DToC Read Only"),
 *   field_types = {
 *     "file",
 *     "media"
 *   }
 * )
 */
class DtocViewerWidget extends FormatterBase {

  /**
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param $langcode
   * @return array
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $mid = '';
    $rev_id = '';

    $route_match = \Drupal::routeMatch();
    if ($route_match->getRouteName() == "entity.node.revision") {
      $current_path = \Drupal::service('path.current')->getPath();
      $path_args = explode('/', $current_path);
      $nid = $path_args[2];
      $rev_id = $path_args[4];
    }

    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof NodeInterface) {
      $mediaStorage = \Drupal::entityTypeManager()->getStorage('media');
      $mids = $mediaStorage->getQuery()
        ->accessCheck(FALSE)
        ->condition('bundle', 'document')
        ->condition('field_media_of', $node->id())
        ->sort('created', 'DESC')
        ->execute();

      if (!empty($mids)) {
        $media_id = reset($mids);
        if ($media_id) {
          $mid = $media_id;
        }
      }
    }

    if ($node->bundle() == 'islandora_object') {
      if (isset($node->get('field_resource_type')->entity)) {
        $resource_type = $node->get('field_resource_type')->entity->getName();
        // If resource type is Text.
        if ($resource_type == 'Text') {
          $model = $node->get('field_model')->entity->getName();
          // If model type is Digital Document.
          if ($model == 'Digital Document') {
            if (!empty($node->get('field_display_hints')->target_id)) {
              $term = Term::load($node->get('field_display_hints')->target_id);
              $field_display_hint_value = $term->getName();
              if ($field_display_hint_value == 'DToC') {
                foreach ($items as $delta => $item) {
                  $elements[$delta] = [
                    '#theme' => 'leaf_dtoc_view_formatter',
                    '#mid' => $mid,
                    '#nid' => $node->id(),
                    '#rev_id' => $rev_id,
                  ];
                }
              }
            }
          }
        }
      }
    }

    $elements['#cache']['max-age'] = 0;

    return $elements;
  }

}
