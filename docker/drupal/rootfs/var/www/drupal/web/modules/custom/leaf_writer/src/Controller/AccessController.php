<?php

/**
 * @file
 * Contains Drupal\leafwriter\Form\LeafWriterConfigurationForm.
 */

namespace Drupal\leaf_writer\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\node\NodeInterface;
use Drupal\group\Entity\GroupContent;
use Drupal\user\Entity\User;

/**
 * Builds an example page.
 */
class AccessController {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(NodeInterface $node) {
    // If node is repository item.
    if ($node->bundle() == 'islandora_object') {
      // Allow access to group editor and contributors as well.
      // Get group id from current node.
      $user_has_access = FALSE;
      $user_is_admin = FALSE;
      $group_content = GroupContent::loadByEntity($node);
      $user = User::load(\Drupal::currentUser()->id());
      $roles = $user->getRoles();
      if (isset($node->get('field_resource_type')->entity)) {
        $resource_type = $node->get('field_resource_type')->entity->getName();
        // If resource type is Text.
        if ($resource_type == 'Text') {
          // If model type is Digital Document.
          $model = $node->get('field_model')->entity->getName();
          if ($model == 'Digital Document') {
            $has_xml_media = leaf_writer_node_has_xml_media($node);
            $display_hint = $node->field_display_hints->getValue();
            if (empty($display_hint) && $has_xml_media) {
              if (in_array('administrator', $roles)) {
                $user_is_admin = TRUE;
              }
            }
          }
        }
      }
      // Check group user access.
      if (!empty($group_content)) {
        $group_content = reset($group_content);
        $group = $group_content->getGroup();
        // Check if current user is a group member.
        $user_member = $group->getMember($user);
        if ($user_member) {
          $user_member_roles = $user_member->getRoles();
          $user_member_role_keys = array_keys($user_member_roles);
          if (in_array('project-editor', $user_member_role_keys) || in_array('project-contributor', $user_member_role_keys)) {
            $user_has_access = TRUE;
          }
        }
      }
      // If user is editor/contributor in group or admin then access allowed.
      if ($user_has_access || $user_is_admin) {
        return AccessResult::allowed();
      }
    }
    return AccessResult::forbidden();
  }

}
