<?php

namespace Drupal\leaf_workflow\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Note that the second parameter of setRequirement() is a string.
    if ($route = $collection->get('view.manage_about.page_1')) {
      $route->setRequirement('_custom_access', '\Drupal\leaf_workflow\Controller\AccessController::access');
    }
  }

}
