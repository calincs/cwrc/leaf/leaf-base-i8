/*jslint browser: true, esversion: 6 */
/*global Mirador, Drupal, once*/
/**
 * @file
 * Displays Mirador viewer.
 */
(function ($, Drupal, once) {
    'use strict';

    /**
     * Initialize the Mirador Viewer.
     */
    Drupal.behaviors.Mirador = {
        attach: function (context, settings) {
            Object.entries(settings.mirador.viewers).forEach(entry => {
              const [base, values] = entry;
              values.windows[0].thumbnailNavigationPosition = 'far-right';
              once('mirador-viewer', base, context).forEach(() =>
                Mirador.viewer(values, window.miradorPlugins || {})
              );
            });
        
            Drupal.behaviors.Mirador.resizeViewer();
            
            $('a#toolbar-item-administration').on('click', function () {
              Drupal.behaviors.Mirador.resizeViewer();
            });
        },
        detach: function (context, settings) {
            Object.entries(settings.mirador.viewers).forEach(entry => {
              const [base, ] = entry;
              once.remove('mirador-viewer', base, context);
            });
        },
        resizeViewer: function () {
          // Set the height of the element to match the screen size
          setTimeout(function () {
            var screenHeigth = window.innerHeight;
            var toolbarHeight = ($('.toolbar-fixed').length > 0) ? 60 : 0;
            toolbarHeight = ($('.toolbar-tray-open').length > 0) ? 110 : toolbarHeight;
            var viewerHeight = screenHeigth - toolbarHeight;
            console.log(screenHeigth, toolbarHeight, viewerHeight);
            $('[data-once="mirador-viewer"]').height(viewerHeight);

            // resize the canvas element
            var canvasHeight = $('[data-once="mirador-viewer"] .mirador-primary-window').height();
            $('[data-once="mirador-viewer"] canvas').height( canvasHeight - 80 );
          }, 250);
        }
    };

})(jQuery, Drupal, once);
