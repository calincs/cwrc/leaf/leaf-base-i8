<?php
namespace Drupal\leaf_baseline\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for project themes.
 */
class ProjectThemesController extends ControllerBase {

  /**
   * Returns a theme page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function themePage() {
    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('group', 'project');
    if (isset($field_definitions['field_group_theme'])) {
        $allowed_options = options_allowed_values($field_definitions['field_group_theme']->getFieldStorageDefinition());
    }

    array_shift($allowed_options);
    return [
        '#theme' => 'project_theme_page',
        '#color_options' => $allowed_options,
    ];
  }

}