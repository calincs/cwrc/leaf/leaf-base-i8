<?php

namespace Drupal\gprofile\Plugin\GroupContentEnabler;

use Drupal\profile\Entity\ProfileType;
use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Derives subgroup plugin definitions based on group types types.
 *
 * @see \Drupal\gprofile\Plugin\GroupContentEnabler\GroupProfile;
 */
class GroupProfileDeriver extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (ProfileType::loadMultiple() as $name => $profile_type) {
      $label = $profile_type->label();

      $this->derivatives[$name] = [
          'entity_bundle' => $name,
          'label' => t('Profile (@type)', ['@type' => $label]),
          'description' => t('Adds %type profile to groups both publicly and privately.', ['%type' => $label]),
        ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }
}
