<?php

namespace Drupal\leaf_baseline\Plugin\views\field;

use Drupal\Core\Render\Markup;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;

/**
 * Current group member timezone field value.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("group_member_timezone")
 */
class GroupMemberTimezone extends FieldPluginBase {

  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->currentDisplay = $view->current_display;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if (\Drupal::currentUser()->isAnonymous()) {
      return;
    }

    $account = \Drupal\user\Entity\User::load($values->users_field_data_group_content_field_data_uid);
    $timezone = $account->getTimeZone();

    return $timezone;
  }

}
