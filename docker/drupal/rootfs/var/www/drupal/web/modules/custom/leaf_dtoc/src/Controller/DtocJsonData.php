<?php

namespace Drupal\leaf_dtoc\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\user\Entity\User;

/**
 * Build JSON object for DToC page.
 */
class DtocJsonData extends ControllerBase {

  /**
   * Get data.
   */
  public function getData() {
    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $mid = $path_args[2];
    $rev_id = '';
    if (!empty($path_args[4])) {
      $rev_id = $path_args[4];
    }

    $media_content = $this->getMediaDtocObject($mid, $rev_id);

    $build = [
      '#theme' => 'dtoc_ifram_content',
      '#media_var' => $media_content,
    ];
    $build['#attached']['drupalSettings']['leafDToC']['json_data'] = $media_content;

    return $build;
  }

  /**
   * Get media dtoc object.
   */
  public function getMediaDtocObject($mid = '', $rev_id = '') {
    $supported_mime_types = [];
    $elements = json_encode([]);
    $media = \Drupal::entityTypeManager()->getStorage('media')->load($mid);

    if (!empty($rev_id)) {
      $result = \Drupal::database()->select('leaf_workflow', 'c')
        ->fields('c', ['media_json_value'])
        ->condition('c.revision_id', $rev_id, '=')
        ->execute()
        ->fetchAssoc();

      if (!empty($result)) {
        $media_array = json_decode($result['media_json_value'], TRUE);

        foreach ($media_array as $media_id => $media_rev_id) {
          $media = \Drupal::entityTypeManager()->getStorage('media')->loadRevision($media_rev_id);
        }
      }
    }

    if ($media == NULL) {
      return $elements;
    }
    $file_id = $media->hasField('field_media_document') ? $media->field_media_document->getValue()[0]['target_id'] : '';

    $file = !empty($file_id) ? File::load($file_id) : [];
    if (!$file || !($file instanceof FileInterface)) {
      return $elements;
      // Throw new \InvalidArgumentException('Invalid file argument provided for #file parameter.');.
    }

    $supported_mime_types = ['application/json'];
    if (!in_array($file->getMimeType(), $supported_mime_types)) {
      return $elements;
      // Throw new \InvalidArgumentException('File not supported for leaf viewer.');.
    }

    // Set the leaf writer read only data attributes.
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $uri = $file->getFileUri();

    $full_path = \Drupal::service('file_url_generator')->generateString($uri);

    $user = User::load(\Drupal::currentUser()->id());
    $json_data = [];

    if (file_exists($uri)) {
      $json_data = trim(file_get_contents($uri));
    }

    return $json_data;
  }

}
