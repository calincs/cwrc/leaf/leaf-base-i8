  /**
   * @type {Drupal~behavior}
   */
(function ($, Drupal) {
  Drupal.behaviors.leafWriterClearcache = {
    attach: function (context) {
      $( ".logout a, .nav-link--user-logout" ).on( "click", function() {
        window.Leafwriter.Leafwriter.clearDB();
      });
    }
  };
})(jQuery, Drupal);