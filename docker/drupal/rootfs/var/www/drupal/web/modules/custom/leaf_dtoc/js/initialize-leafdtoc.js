/**
 * @file
 * Initialize the leaf dtoc library.
 */

(($, Drupal, once, drupalSettings) => {
  /**
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.leafdtocInitializer = {
    attach: function (context) {
      var jsonBody = drupalSettings.leafDToC.json_data;
      var jsonBodyObj = JSON.parse(jsonBody);

      var pathname = window.location.pathname; 
      var nid = pathname.split('/')[3];

      // window.dtocApp = new dtoc(jsonBodyObj);
      const dtocApp = new dtoc(jsonBodyObj);

      dtocApp.subscribe('allTagsLoaded', () => {
        const inputConfig = dtocApp.getInputConfig();
        sessionStorage.setItem("dtocInputConfig" + nid, JSON.stringify(inputConfig));
      });
    }
  };
})(jQuery, Drupal, once, drupalSettings);
