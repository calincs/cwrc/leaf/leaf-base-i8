<?php

namespace Drupal\leaf_workflow\Plugin\views\field;

use Drupal\Core\Render\Markup;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;

/**
 * Workflow revision revert link field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("workflow_user_revision_revert_link")
 */
class WorkflowRevisionRevertLink extends FieldPluginBase {

  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->currentDisplay = $view->current_display;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if (\Drupal::currentUser()->isAnonymous()) {
      return;
    }

    $latest_vid = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->getLatestRevisionId($values->leaf_workflow_node_id);

    if ($latest_vid == $values->leaf_workflow_revision_id) {
      $result = " | <span>Current</span>";
    }
    else {
      $link = "/node/$values->leaf_workflow_node_id/revisions/$values->leaf_workflow_revision_id/revert";
      $result = " | <a href=$link>Restore</a>";
    }

    return [
      '#markup' => Markup::create($result),
    ];
  }

}
