/**
 * @file
 * Overwrite the default Displays OpenSeadragon viewer javascript.
 */
(function ($, Drupal, once) {
  'use strict';

  /**
   * The DOM element that represents the Singleton Instance of this class.
   * @type {string}
   */
  let base = '#openseadragon-viewer';

  const getTileSources = async (manifestUrl) => {
    const response = await fetch(manifestUrl);
    const manifest = await response.json();
    let data = [];
    if (manifest && manifest.hasOwnProperty('sequences')) {
      manifest.sequences.forEach(
        (sequence) => {
          if (!sequence.hasOwnProperty('canvases')) {
            return;
          }
          sequence.canvases.forEach(
            (canvas) => {
              if (!canvas.hasOwnProperty('images')) {
                return;
              }
              canvas.images.forEach(
                (image) => {
                  data.push(image.resource.service['@id'])
                }
              );
            }
          );
        }
      );
    }
    return data;
  };

  const openSeadragonOverwriteHandler = () => {
    once('openSeadragonViewer', '[data-open-seadragon-settings]').forEach(
      async (viewerContainer) => {
        const settings = JSON.parse(viewerContainer.dataset.openSeadragonSettings);
        if (settings.options.tileSources.length === 0) {
          settings.options.tileSources = await getTileSources(settings.manifestUrl);
        }

        settings.options.sequenceMode = settings.options.tileSources.length > 1 && !settings.options.collectionMode;
        base = `#${viewerContainer.id}`;
        Drupal.openSeadragonViewer[base] = new Drupal.openSeadragonViewer(base, settings);
      },
    );
  };

  /**
   * Initialize the OpenSeadragon Viewer.
   */
  Drupal.behaviors.leafBaselineOpenSeadragon = {
    attach: function (context, settings) {
      if (settings.hasOwnProperty('openseadragon')) {
        Object.keys(settings.openseadragon).forEach(function (osdViewerId) {
          // Use custom element #id if set.
          base = '#' + osdViewerId;
          $(once('openSeadragonViewer', base, context)).each(function () {
            Drupal.openSeadragonViewer[base] = new Drupal.openSeadragonViewer(base, settings.openseadragon[osdViewerId]);
          });
        });
        return;
      }
      // Using our custom overwrite handler to initialize openseadragon.
      openSeadragonOverwriteHandler();
    },
    detach: function () {
      $(base).removeClass('openSeadragonViewer-processed');
      $(base).removeData();
      $(base).off();
      delete Drupal.openSeadragonViewer[base];
    }
  };

  /**
   * Creates an instance of the OpenSeadragon Viewer widget.
   *
   * @param {string} base
   *   The element ID that this class is bound to.
   * @param {object} settings
   *   Drupal.settings for this object widget.
   *
   * @constructor
   */
  Drupal.openSeadragonViewer = function (base, settings) {
    const viewer = new OpenSeadragon(settings.options);

    const update_clip = function (event) {
      const viewer = event.eventSource;
      const fitWithinBoundingBox = function (d, max) {
        if (d.width / d.height > max.x / max.y) {
          return new OpenSeadragon.Point(max.x, parseInt(d.height * max.x / d.width));
        }
        return new OpenSeadragon.Point(parseInt(d.width * max.y / d.height), max.y);
      };

      const getDisplayRegion = function (viewer, source) {
        // Determine portion of scaled image that is being displayed.
        const box = new OpenSeadragon.Rect(0, 0, source.x, source.y);
        const container = viewer.viewport.getContainerSize();
        const bounds = viewer.viewport.getBounds();
        // If image is offset to the left.
        if (bounds.x > 0) {
          box.x = box.x - viewer.viewport.pixelFromPoint(new OpenSeadragon.Point(0, 0)).x;
        }
        // If full image doesn't fit.
        if (box.x + source.x > container.x) {
          box.width = container.x - viewer.viewport.pixelFromPoint(new OpenSeadragon.Point(0, 0)).x;
          if (box.width > container.x) {
            box.width = container.x;
          }
        }
        // If image is offset up.
        if (bounds.y > 0) {
          box.y = box.y - viewer.viewport.pixelFromPoint(new OpenSeadragon.Point(0, 0)).y;
        }
        // If full image doesn't fit.
        if (box.y + source.y > container.y) {
          box.height = container.y - viewer.viewport.pixelFromPoint(new OpenSeadragon.Point(0, 0)).y;
          if (box.height > container.y) {
            box.height = container.y;
          }
        }
        return box;
      };

      const source = viewer.source;
      const zoom = viewer.viewport.getZoom();
      const size = new OpenSeadragon.Rect(0, 0, source.dimensions.x, source.dimensions.y);
      const container = viewer.viewport.getContainerSize();
      const fitSource = fitWithinBoundingBox(size, container);
      const totalZoom = fitSource.x / source.dimensions.x;
      const container_zoom = fitSource.x / container.x;
      const level = (zoom * totalZoom) / container_zoom;
      const box = getDisplayRegion(viewer, new OpenSeadragon.Point(parseInt(source.dimensions.x * level), parseInt(source.dimensions.y * level)));
      const scaledBox = new OpenSeadragon.Rect(parseInt(box.x / level), parseInt(box.y / level), parseInt(box.width / level), parseInt(box.height / level));
      const params = {
        'identifier': source['@id'],
        'region': scaledBox.x + ',' + scaledBox.y + ',' + (scaledBox.getBottomRight().x - scaledBox.x) + ',' + (scaledBox.getBottomRight().y - scaledBox.y),
        'size': (zoom <= 1) ? source.dimensions.x + ',' + source.dimensions.y : container.x + ',' + container.y
      };
    };

    viewer.addHandler("open", update_clip);
    viewer.addHandler("animation-finish", update_clip);

    if (settings.fitToAspectRatio) {
      viewer.addHandler("open", function (event) {
        const viewer = event.eventSource;
        if (viewer.source.aspectRatio / viewer.viewport.getAspectRatio() <= 1) {
          viewer.viewport.fitVertically();
        }
        else {
          viewer.viewport.fitHorizontally();
        }
      });
    }

  };
})(jQuery, Drupal, once);