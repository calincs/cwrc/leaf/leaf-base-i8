<?php

namespace Drupal\leaf_baseline\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\node\NodeInterface;

/**
 * Provides a 'Node Page Header' Block.
 *
 * @Block(
 *   id = "node_page_header_block",
 *   admin_label = @Translation("Node Page Header Block (Title & Logo)"),
 *   category = @Translation("Node Page Header Block (Title & Logo)"),
 * )
 */
class NodePageHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof NodeInterface) {
      $nid = $node->id();
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
      $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

      if ($node->hasTranslation($langcode)) {
        $node = $node->getTranslation($langcode);
      }

      $current_path = \Drupal::service('path.current')->getPath();
      $path_explode = explode('/', $current_path);

      // Override node object if it's revision node page.
      if (!empty($path_explode[3]) && $path_explode[3] == 'revisions' && is_numeric($path_explode[4])) {
        $vid = $path_explode[4];
        $node = \Drupal::entityTypeManager()->getStorage('node')->loadRevision($vid);
      }

      $group = get_groups_object_from_node($nid);
      if (!empty($group)) {
        // Take first group if it's attached to multiple.
        $group = $group[0];

        $logo_container = "";
        if (!empty($group->field_logo->entity)) {
          if (!empty($group->field_logo->entity->getFileUri())) {
            $image_url = \Drupal::service('file_url_generator')->generateAbsoluteString($group->field_logo->entity->getFileUri());
            $logo_container = "<div class='group-logo'><img src='" . $image_url . "' width='100%' height='auto'></div>";
          }
        }

        $group_markup = "<a href='/group/" . $group->id() . "'><div class='logo-label-wrapper'>" . $logo_container . "<div class='group-title block-page-title-block'><h1>" . leaf_baseline_trim_words($node->label(), 8) . "</h1></div></div></a>";
      }
      else {
        $group_markup = "<a href='/node/" . $nid . "'><div class='group-title block-page-title-block'><h1>" . leaf_baseline_trim_words($node->label(), 8) . "</h1></div></a>";
      }
    }

    return [
      '#markup' => $group_markup,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // With this when your node change your block will rebuild.
    if ($group = \Drupal::routeMatch()->getParameter('group')) {
      // If there is node add its cachetag.
      return Cache::mergeTags(parent::getCacheTags(), ['group:' . $group->id()]);
    }
    elseif ($node = \Drupal::routeMatch()->getParameter('node')) {
      // If there is node add its cachetag.
      return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id()]);
    }
    else {
      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // If you depends on \Drupal::routeMatch()
    // you must set context of this block with 'route' context tag.
    // Every new route this block will rebuild.
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

}
