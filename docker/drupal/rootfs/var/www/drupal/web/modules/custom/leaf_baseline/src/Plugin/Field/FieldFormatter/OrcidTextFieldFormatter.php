<?php

namespace Drupal\leaf_baseline\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Plugin implementation of the 'Random_default' formatter.
 *
 * @FieldFormatter(
 *   id = "orcid_textfieldformatter",
 *   label = @Translation("Orcid Field Formatter"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class OrcidTextFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      if ($this->getSetting('as_link')) {
        $url = Url::fromUri('https://orcid.org/'.$item->value);
        if ($this->getSetting('open_new_window')) {
          $url->setOptions(['attributes' => ['target' => '_blank']]);
        }
        $orcid_link = Link::fromTextAndUrl($item->value, $url);
        $element[$delta] = ['#markup' => $orcid_link->toString()];
      }
      else {
        $element[$delta] = ['#markup' => $item->value];
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        // Create the custom setting 'open_new_window', and
        // assign a default value of TRUE
        'open_new_window' => TRUE,
        'as_link' => TRUE,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['open_new_window'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open in new window'),
      '#default_value' => $this->getSetting('open_new_window'),
    ];
    $element['as_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display as Link'),
      '#default_value' => $this->getSetting('as_link'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $setting = ($this->getSetting('open_new_window')) ? $this->t('Yes') : $this->t('No');
    $as_link = ($this->getSetting('as_link')) ? $this->t('Yes') : $this->t('No');
    $summary[] = $this->t('Open in New Window: @window', array('@window' => $setting));
    $summary[] = $this->t('Display as link: @link', array('@link' => $as_link));
    return $summary;
  }

}
