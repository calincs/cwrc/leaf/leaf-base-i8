<?php

namespace Drupal\leaf_taxonomy\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 *
 */
class TaxonomyController extends ControllerBase {

  /**
   *
   */
  public function getTitle(RouteMatchInterface $route_match) {
    $field_name = \Drupal::routeMatch()->getParameter('field_name');
    $bundle = \Drupal::routeMatch()->getParameter('entity_bundle');

    $bundle_fields = \Drupal::getContainer()->get('entity_field.manager')->getFieldDefinitions('node', $bundle);
    $label = $bundle_fields[$field_name]->getLabel();

    return 'Add ' . $label;
  }

}
