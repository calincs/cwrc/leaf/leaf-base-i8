<?php

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\group\Entity\GroupContentType;
use Drupal\group\Entity\GroupInterface;
use Drupal\profile\Entity\Profile;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\profile\Entity\ProfileType;
use Drupal\user\Entity\User;

/**
 * Implements hook_ENTITY_access().
 *
 * When trying to view, update or delete a profile it suffices to have the right to
 * do so in only one group the profile belongs to. If you wish to prevent any such
 * action on your own terms, implement hook_profile_access() in your module.
 *
 */
function gprofile_profile_access(ProfileInterface $profile, $op, AccountInterface $account) {
  // We do not care about create access as we have our own wizard for that. Any
  // operation aside from 'view', 'update' and 'delete' is also unsupported.
  if (!in_array($op, ['view', 'update', 'delete'])) {
    return AccessResult::neutral();
  }

  // Some modules, including the code in \Drupal\node\NodeForm::access() may
  // check for 'view', 'update' or 'delete' access on new nodes, even though
  // that makes little sense. We need to account for it to avoid crashes because
  // we would otherwise query the DB with a non-existent node ID.
  if ($profile->isNew()) {
    return AccessResult::neutral();
  }

  $profile_type = $profile->bundle();
  $plugin_id = 'group_profile:' . $profile_type;

  // Load all of the group content for this node.
  $group_contents = \Drupal::entityTypeManager()
    ->getStorage('group_content')
    ->loadByEntity($profile);

  $group_contents += \Drupal::entityTypeManager()
    ->getStorage('group_content')
    ->loadByEntity($profile->getOwner());

  // If the profile or profiles user does not belong to any group, we have nothing to say.
  if (empty($group_contents)) {
    return AccessResult::neutral();
  }

  /** @var GroupInterface[] $groups */
  $groups = [];
  foreach ($group_contents as $group_content) {
    /** @var GroupContentInterface $group_content */
    $group = $group_content->getGroup();
    $groups[$group->id()] = $group;
  }

  // From this point on you need group to allow you to perform the operation.
  switch ($op) {
    case 'view':
      foreach ($groups as $group) {
        if ($profile->isPublished()) {
          if (
            $group->hasPermission("view $plugin_id entity", $account) ||
            $group->hasPermission("view $profile_type member profiles", $account)
          ) {
            $access_result = AccessResult::allowed();
          }
        }
        elseif (
          $group->hasPermission("view unpublished $plugin_id entity", $account) ||
          $group->hasPermission("view unpublished $profile_type member profiles", $account)
        ) {
          $access_result = AccessResult::allowed();
        }
      }
      break;

    case 'update':
    case 'delete':
      foreach ($groups as $group) {
        if ($group->hasPermission("$op any $plugin_id entity", $account)) {
          $access_result = AccessResult::allowed();
        }
        elseif ($account->id() == $profile->getOwnerId() && $group->hasPermission("$op own $plugin_id entity", $account)) {
          $access_result = AccessResult::allowed();
        }
      }
      break;
  }

  // Instead of outright forbidding access when no group granted it, we return
  // a neutral access result to play nice with other modules. If the end result
  // is still neutral, Drupal will deny access anyway unless the node grants
  // system allows the operation in a last ditch effort to determine access.
  if (!isset($access_result)) {
      $access_result = AccessResult::neutral();
  }

  $access_result->addCacheableDependency($profile->getOwner());
  $access_result->addCacheableDependency($account);

  return $access_result;
}
