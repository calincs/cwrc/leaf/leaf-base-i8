#!/usr/bin/env bash
# shellcheck shell=bash
set -e

# Allow nginx to query this services status (for install page).
s6-svperms -g nginx /run/service/nginx

# Show an install page until ready.
rm -f /etc/nginx/sites-enabled/default.conf
ln -f -s /etc/nginx/http.d/install.conf /etc/nginx/sites-enabled/install.conf
exec /usr/sbin/nginx &
PID=$!

/etc/s6-overlay/scripts/drupal-install.sh

kill $PID
wait
#exec /usr/sbin/nginx -s stop

rm -f /etc/nginx/sites-enabled/install.conf
ln -f -s /etc/nginx/http.d/default.conf /etc/nginx/sites-enabled/default.conf