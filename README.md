# LEAF Base i8 <!-- omit in toc -->

- [Description](#description)
- [Requirements](#requirements)
- [Development](#development)
  - [Workflow](#workflow)
  - [Drupal Development](#drupal-development)
  - [IDE](#ide)
  - [PHPStorm](#phpstorm)
- [Related Resources](#related-resources)
- [Sponsoring Institutions](#sponsoring-institutions)
- [Staff](#staff)
- [Partners](#partners)
- [Funders](#funders)

## Description

The _Linked Academic Editing Framework (LEAF)_ is a Virtual research Environment
based on the [Islandora 2.0] digital asset management framework.

LEAF aims to address the challenges that face many who undertake and maintain
large-scale collaborative digital humanities projects. It supports scalability,
interoperability, and preservation while allowing for dynamic, iterative, and
collaborative editing to ensure that our materials, collections, and editions
will remain current, viable, and accessible.

LEAF is a collaboration to extend the [Canadian Writing Research Collaboratory]
(CWRC), built by the Universities of Alberta and Guelph, which launched in 2016.
The LEAF platform combines hardware, software, and personnel. LEAF is being
built on a solid foundation in its data models, core functionality, and code
management, so that it is positioned for extension and long-term sustainability.
The platform is based on the Islandora 2.0 framework, which combines Drupal 9
with a Fedora 5 repository for long-term preservation.

The LEAF repository will customize and enhance Islandora to enable digital
humanities workflows and publication needs. Enhancements include an innovative
web-based editing tool that allows users to employ TEI XML along with Web
Annotation and IIIF standards-compatible Linked Open Data annotations that
enhance discoverability and interoperability. LEAF is offered as containerized
open-source code, available for download and installation by other institutions.
It will support the production and publication of dynamic digital scholarly
editions and collections, offering multilingual transcription, translation, and
image markup. Entirely browser-based, its functionality includes an in-browser
XML markup editor (LEAF-Writer, built on CWRC-Writer, which can be installed as
a standalone editing environment), XML rendering tools, built-in text and data
visualization tools including the Voyant Tools suite and its Dynamic Table of
Contexts Browser. LEAF will provide a sophisticated interface for digital
content in which XML markup is leveraged for navigation and active reading, and
enhanced with Linked Open Data. LEAF is partnered with the
[Linked Infrastructure for Networked Cultural Scholarship] (LINCS)
cyberinfrastructure project, as part of a larger linked open data ecosystem for
research.

See [Technical documentation] for installation and code management instructions.

## Requirements

The following tools are required to develop locally:

- [pre-commit](https://pre-commit.com/#install)

Additionally the following unix based tools are required, and should be
available by default on most system or though the system package manager.

- [jq](https://stedolan.github.io/jq/)
- `awk`
- `git`
- `make` (*GNU Make*)

If you intend to work using docker compose for Drupal development then the
following tools are required to develop locally.

- [Docker 20.10+](https://docs.docker.com/get-docker/)
- [Docker Compose 2.0.1+](https://docs.docker.com/compose/install/)
- [mkcert](https://github.com/FiloSottile/mkcert#installation)

If you do not intend to work on Kubernetes and the Helm charts the following
tools are required to develop locally:

- [Helm](https://helm.sh/docs/intro/install/)
- [Helmfile](https://helmfile.readthedocs.io/en/latest/#installation)
- [Kubernetes 1.23+](https://kubernetes.io/docs/tasks/tools/)
- [mkcert](https://github.com/FiloSottile/mkcert#installation)

> N.B. You do not need to install Kubernetes if using Docker Desktop as it is
> included, which you can enabled using the instructions
> [here](https://docs.docker.com/desktop/kubernetes/)

After `helm` is successfully installed.

Please install the following plugins:

```bash
helm plugin install https://github.com/jkroepke/helm-secrets
helm plugin install https://github.com/databus23/helm-diff
```

## Development

A `Makefile` is provided for convenience to aid in local development.

```txt
Usage:
  make <target>

General:
  push                           Builds and Pushes cross platform images to GitLab registry.
  clean                          Destroys local environment and cleans up any uncommitted files.
  help                           Displays this help message.

Docker Compose:
  compose-build                  Builds local images from the 'docker' folder.
  compose-pull                   Pull service images from 'docker-compose.yml'.
  compose-up                     Starts up the local development environment.
  compose-down                   Stops the local development environment.

Helmfile:
  helmfile-apply                 Uses helmfile to installs the dependencies required for local development into the active Kubernetes cluster.
  helmfile-destroy               Destroy resources created by the helmfile from your active Kubernetes cluster.

Helm:
  helm-install                   Installs/updates the Helm chart into the active Kubernetes cluster.
  helm-uninstall                 Uninstall the Helm chart from your active Kubernetes cluster.
  helm-package                   Packages the Helm chart into a versioned chart archive file in the build directory.
  helm-lint                      Performs a lint check on the helm charts in the chart folder.
  helm-test                      Render chart templates locally and display the output.
  helm-template                  Render chart templates locally and display the output (Helpful when debugging).

Kubectl:
  kubectl-switch-namespace       Change the default namespace for kubectl to the namespace used for resources created by helm.
  kubectl-delete-pvc             Deletes all persistent volume claims in the namespace used by helm.
```

### Workflow

Typically after cloning you can:
* Docker: run `make compose-up` which will build the docker images (and SSL certificates) and run them with `docker compose`. If successful you should see the following after about 2-3 minutes.
* Or K8S local: run `make hemfile-apply` (to deploy nginx ingress, cert-manager, and k8s dashboard https://dashboard.islandora.dev) and `make helm-install` (to deploy LEAF containers in the `./chart`).

The available endpoints on a Docker development workflow are as follows. On a local k8s deploy, the Solr and ActiveMQ ingress routes are disabled but can be enabled in the Helm Chart values.yaml file. For a CI/CD deploy, the GitLab auto deploy values file sets the ingress for Solr and ActiveMQ as disabled.


```text
  Credentials:
  Username                       admin
  Password                       password

  Services Available:
  http://activemq.islandora.dev  The ActiveMQ administrative console.
  http://islandora.dev           The Drupal website.
  http://islandora.dev/matomo/   The Matomo (web analytics) website.
  http://ide.islandora.dev       The in browser editor.
  http://solr.islandora.dev      The Solr search engine administrative console.
  http://traefik.islandora.dev   The Traefik router administrative console.
```

### Drupal Development

A few directories and files are bind mounted into the `drupal` / `ide`
containers, from the [./docker/drupal/rootfs/var/www/drupal] directory.

| Directory / File                                                                | Description                                                   |
| :------------------------------------------------------------------------------ | :------------------------------------------------------------ |
| [assets/](./docker/drupal/rootfs/var/www/drupal/assets)                         | Files referenced by `composer` during install.                |
| [patches/](./docker/drupal/rootfs/var/www/drupal/patches)                       | Patches `composer` applies to modules, etc.                   |
| [config/](./docker/drupal/rootfs/var/www/drupal/config)                         | Drupal site configuration.                                    |
| [content/](./docker/drupal/rootfs/var/www/drupal/content)                       | `content_sync` entities used to populate the site on install. |
| [web/modules/custom/](./docker/drupal/rootfs/var/www/drupal/web/modules/custom) | Custom Drupal modules managed by this repository.             |
| [web/themes/custom/](./docker/drupal/rootfs/var/www/drupal/web/themes/custom)   | Custom Drupal themes managed by this repository.              |
| [composer.json](./docker/drupal/rootfs/var/www/drupal/composer.json)            | `composer` dependencies.                                      |
| [composer.lock](./docker/drupal/rootfs/var/www/drupal/composer.lock)            | Lock file for `composer`.                                     |

### IDE

An `IDE` is provided at <http://ide.islandora.dev> which includes:

- Intellisense & Code Completion
- Build tasks for performing lints & other common actions (accessible via `CTRL-B` or `CMD-B`)
- Integrated Debugger (`XDebug`)
- `PHPCS` / `PHPCBF`
- etc...

To enable `XDebug` when using `drush` via the built-in terminal enter the
following command before invoking `drush`:

```bash
export XDEBUG_SESSION=1
```

For web requests, you must also send an `XDEBUG_SESSION` cookie with your
request, this can be toggled on and off via a browser plugin such as the
following.

- [Chrome](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc?hl=en)
- [Firefox](https://addons.mozilla.org/en-GB/firefox/addon/xdebug-helper-for-firefox/)


### PHPStorm

PHPStorm and alternative IDE's which allow for remote development via SSH are
also supported.

Add the following to your `~/.ssh/config` file:

```txt
Host islandora.dev
  ForwardAgent yes
  PasswordAuthentication yes
  Port 2222
  PreferredAuthentications password
  PubkeyAuthentication no
  StrictHostKeyChecking no
  User nginx
  UserKnownHostsFile /dev/null
```

You should now be able to ssh like so (assuming you've already brought the
docker compose environment up):

```bash
ssh islandora.dev
```

You can then connect via the PHP remote development feature.

![PHPStorm](./docs/assets/phpstorm.gif)

> N.B. PHPStorm remote is not supported form Arm architectures so the above will
> not work on M1 macbooks and later.

## Related Resources

- [Islandora Foundation](https://islandora.ca/)
- [ISLE](https://islandora-collaboration-group.github.io/ISLE/about/about-isle/)

## Sponsoring Institutions

- [Bucknell University](https://www.bucknell.edu/) (Diane Jakacki)
- [Newcastle University](https://www.ncl.ac.uk/) (James Cummings)
- [University of Guelph](https://www.uoguelph.ca/) (Susan Brown)

## Staff

- Assistant director: Mihaela Ilovan
- Technical director: Jeff Antoniuk
- Senior Drupal Developer: Nia Kathoni
- Javascript developer and designer: Luciano Dos Reis Frizzera
- Design: Jennifer Blair
- Research associate: Carolyn Black

## Partners
- Born Digital
- NGB Software Consulting

## Funders
- Andrew W. Mellon Foundation
- CANARIE
- Canada Foundation for Innovation

[./docker/drupal/rootfs/var/www/drupal]: ./docker/drupal/rootfs/var/www/drupal
[Canadian Writing Research Collaboratory]: https://cwrc.ca
[Islandora 2.0]: https://www.drupal.org/project/islandora/releases/2.0.0
[Linked Infrastructure for Networked Cultural Scholarship]: https://lincsproject.ca/
[Technical documentation]: https://gitlab.com/calincs/cwrc/leaf/leaf-base-i8/-/wikis/home
