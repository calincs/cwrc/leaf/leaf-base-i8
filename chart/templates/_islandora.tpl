
{{/* vim: set filetype=mustache: */}}

{{- /*
Helpers to define properties for use in islandora.{RESOURCE}.tpl templates.

The properties are derived from {{ .Values.islandora }}.
*/ -}}
{{- define "islandora.activemq.properties" -}}
# There doesn't seem to be an operator that supports classic ActiveMQ only Artemis.
replicas: 1
containers:
  activemq:
    ports:
      http: 8161
      stomp: 61613
      openwire: 61616
    mounts:
      - name: activemq-data
        path: /opt/activemq/data
securityContext:
  fsGroup: 1000
volumes:
  - {{ include "islandora.volume.default.properties" (list "activemq-data" .) | nindent 4 }}
{{- end -}}

{{- define "islandora.alpaca.properties" -}}
containers:
  alpaca:
    ports:
      http: 8181
      ssh: 8101
      rmi: 1099
      jmx: 44444
{{- end -}}

{{- define "islandora.cantaloupe.properties" -}}
containers:
  cantaloupe:
    ports:
      http: 8182
    mounts:
      - name: "cantaloupe-data"
        path: /data
securityContext:
  fsGroup: 1000
volumes:
  - {{ include "islandora.volume.default.properties" (list "cantaloupe-data" .) | nindent 4 }}
{{- end -}}

{{- define "islandora.crayfish.properties" -}}
{{ $name := first . }}
{{ $context := last . }}
containers:
  {{ $name }}:
    ports:
      http: 8000
{{- end -}}

{{- define "islandora.drupal.properties" -}}
{{- /*
  Since Drupal is the connical component it does not have the component suffix.
  This is a GitLab convention, to support "trackable" application.

  https://docs.gitlab.com/ee/user/project/canary_deployments.html
  https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html
  https://docs.gitlab.com/ee/topics/autodevops/customize.html#advanced-replica-variables-setup
*/ -}}
{{- $hostname := (include "hostname" .Values.service.url | replace "\"" "") -}}
fullName: "{{ template "fullname" $ }}"
trackableName: "{{ template "trackableappname" $ }}"
containers:
  drupal:
    ports:
      http: 80
    mounts:
      - name: drupal-public-files
        path: /var/www/drupal/web/sites/default/files
      - name: drupal-private-files
        path: /var/www/drupal/private
    configs:
      - drupal
configs:
  drupal:
    data:
      # Keep this in sync with "docker-compose.yaml".
      DRUPAL_DEFAULT_CANTALOUPE_URL: {{ printf "https://%s/iiif/2" $hostname }}
      DRUPAL_DEFAULT_INSTALL_EXISTING_CONFIG: "true"
      DRUPAL_DEFAULT_MATOMO_URL: {{ printf "https://%s/matomo/" $hostname }}
      DRUPAL_DEFAULT_NAME: "CWRC/CSEC"
      DRUPAL_DEFAULT_PROFILE: "minimal"
      DRUPAL_DEFAULT_SITE_URL: {{ $hostname }}
      DRUPAL_DEFAULT_SOLR_CORE: "default"
      DRUPAL_DRUSH_URI: {{ printf "https://%s" $hostname }}
securityContext:
  fsGroup: 101
volumes:
  - {{ include "islandora.volume.default.properties" (list "drupal-public-files" .) | nindent 4 }}
  - {{ include "islandora.volume.default.properties" (list "drupal-private-files" .) | nindent 4 }}
{{- end -}}

{{- define "islandora.fits.properties" -}}
containers:
  fits:
    ports:
      http: 8080
{{- end -}}

{{- define "islandora.mariadb.properties" -}}
# There isn't any well supported MariaDB specific operators.
# KindDB could potentially be used to allow for more than one replica.
# Or perhaps a different database could be adopted.
replicas: 1
containers:
  mariadb:
    ports:
      mysql: 3306
    mounts:
      - name: "mariadb-data"
        path: /var/lib/mysql
securityContext:
  fsGroup: 101
volumes:
  - {{ include "islandora.volume.default.properties" (list "mariadb-data" .) | nindent 4 }}
{{- end -}}

{{- define "islandora.matomo.properties" -}}
{{- $hostname := (include "hostname" .Values.service.url | replace "\"" "") -}}
containers:
  matomo:
    ports:
      http: 80
configs:
  matomo:
    data:
      # Keep this in sync with "docker-compose.yaml".
      MATOMO_DEFAULT_HOST: {{ printf "https://%s" $hostname }}
{{- end -}}

{{- define "islandora.solr.properties" -}}
# Until we integrate Solr Operator, at most 1 replica is supported ignore user provided values.
replicas: 1
containers:
  solr:
    ports:
      http: 8983
    mounts:
      - name: "solr-data"
        path: /data
securityContext:
  fsGroup: 1000
volumes:
  - {{ include "islandora.volume.default.properties" (list "solr-data" .) | nindent 4 }}
{{- end -}}

{{- /*
Helpers to define properties for use in islandora.{RESOURCE}.tpl templates.

The properties are derived from {{ .Values.islandora }}.
*/ -}}
{{- define "islandora.default.properties" -}}
{{- $name := index . 0 -}}
{{- $context := index . 1 -}}
{{- $config := (get $context.Values.islandora.configs $name) -}}
{{- $container := (get $context.Values.islandora.containers $name) -}}
{{- $workload := (get $context.Values.islandora.workloads $name) -}}
name: {{ $name }}
fullName: "{{ template "fullname" $context }}-{{ $name }}"
trackableName: "{{ template "trackableappname" $context }}-{{ $name }}"
component: {{ $name }}
replicas: {{ $workload.replicas | default 1 }}
{{- if not (empty $container) }}
containers:
  {{ $name }}: {{ include "islandora.container.default.properties" . | nindent 4 }}
    {{- if not (empty $config) }}
    configs:
      - {{ $name }}
    {{- end }}
{{- end }}
{{- if not (empty $config) }}
configs:
  {{ $name }}: {{ include "islandora.config.default.properties" . | nindent 4 }}
{{- end }}
{{- end -}}

{{- define "islandora.config.default.properties" -}}
{{- $name := index . 0 -}}
{{- $context := index . 1 -}}
{{- $data := (get $context.Values.islandora.configs $name) -}}
name: {{ $name }}
data: {{ tpl (toYaml $data) $context | nindent 2 }}
{{- end -}}

{{- define "islandora.volume.default.properties" -}}
{{- $name := index . 0 -}}
{{- $context := index . 1 -}}
{{- $volume := (get $context.Values.islandora.volumes $name) -}}
name: {{ $name }}
resources: {{ tpl (toYaml $volume.resources) $context | nindent 2 }}
{{- end -}}

{{- define "islandora.container.default.properties" -}}
{{- $name := index . 0 -}}
{{- $context := index . 1 -}}
{{- $container := (get $context.Values.islandora.containers $name) -}}
name: {{ $name }}
image: {{ $container.image | default (printf "%s/%s:%s" (tpl $container.repository $context) $name (tpl $container.tag $context)) }}
imagePullPolicy: {{ tpl $container.imagePullPolicy $context }}
resources: {{ tpl (toYaml $container.resources) $context | nindent 2 }}
{{- end -}}

{{- define "islandora.properties" -}}
{{- $name := first . -}}
{{- $context := last . -}}
{{ include "utils.yaml.merge" (list
  (include "islandora.default.properties" (list $name $context))
  (include (printf "islandora.%s.properties" $name) $context))
}}
{{- end -}}

{{- define "islandora.named.properties" -}}
{{- $properties := index . 0 -}}
{{- $name := index . 1 -}}
{{- $context := index . 2 -}}
{{ include "utils.yaml.merge" (list
  (include "islandora.default.properties" (list $name $context))
  (include (printf "islandora.%s.properties" $properties) (list $name $context)))
}}
{{- end -}}

{{- /*
Resource templates to extend the common.{RESOURCE}.tpl resource templates.

The properties are derived from {{ .Values.islandora }}.
*/ -}}

{{- /*
See: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.25/#configmap-v1-core
*/ -}}
{{- define "islandora.configMap.tpl" -}}
metadata:
  name: {{ .name }}
data: {{ toYaml .data | nindent 2 }}
{{- end -}}

{{- /*
See: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.25/#podspec-v1-core
*/ -}}
{{- define "islandora.podSpec.container.tpl" -}}
name: {{ .name }}
image: {{ .image }}
imagePullPolicy: {{ .imagePullPolicy }}
envFrom:
{{- if .context.Values.application.secretName }}
  - secretRef:
      name: {{ .context.Values.application.secretName }}
{{- end }}
{{- range .configs }}
  - configMapRef:
      name: {{ . }}
{{- end }}
resources: {{ toYaml .resources | nindent 2 }}
ports:
{{- range $name, $port := .ports }}
- name: {{ $name | quote }}
  containerPort: {{ $port }}
{{- end }}
volumeMounts:
{{- range .mounts }}
- name: {{ .name }}
  mountPath: {{ .path }}
{{- end }}
{{- end -}}

{{- define "islandora.deployment.tpl" -}}
metadata:
  labels: &labels
    component: {{ .component }}
  name: {{ .trackableName }}
spec:
  replicas: {{ .replicas }}
  selector:
    matchLabels:
      component: {{ .component }}
  template:
    metadata:
      labels:
        <<: *labels
    spec:
      containers:
      {{- range .containers }}
        - {{ include "islandora.podSpec.container.tpl" (set (deepCopy .) "context" $.context) | nindent 10 }}
      {{- end }}
      volumes:
      {{- range .volumes }}
        - name: {{ .name }}
          persistentVolumeClaim:
            claimName: {{ .name }}
      {{- end }}
      {{- if .securityContext }}
      securityContext: {{ .securityContext | toYaml | nindent 8 }}
      {{- end }}
{{- end -}}

{{- define "islandora.service.tpl" -}}
metadata:
  labels:
    component: {{ .component }}
  name: {{ .name }}
spec:
  selector:
    component: {{ .component }}
  ports:
  {{- range .containers }}
  {{- range $key, $value := .ports }}
    - name: {{ $key | quote }}
      port: {{ $value }}
      targetPort: {{ $key | quote }}
  {{- end }}
  {{- end }}
{{- end -}}

{{- define "islandora.statefulSet.tpl" -}}
metadata:
  labels: &labels
    component: {{ .component }}
  name: {{ .trackableName }}
spec:
  replicas: {{ .replicas }}
  selector:
    matchLabels:
      component: {{ .component }}
  serviceName: {{ .fullName }}
  template:
    metadata:
      labels:
        <<: *labels
    spec:
      containers:
      {{- range .containers }}
        - {{ include "islandora.podSpec.container.tpl" (set (deepCopy .) "context" $.context) | nindent 10 }}
      {{- end }}
      {{- if .securityContext }}
      securityContext: {{ .securityContext | toYaml | nindent 8 }}
      {{- end }}
  volumeClaimTemplates:
  {{- range .volumes }}
    - metadata:
        name: {{ .name }}
      spec:
        accessModes: [ "ReadWriteOnce" ]
        resources: {{ .resources | toYaml | nindent 10 }}
  {{- end }}
{{- end -}}

{{- define "islandora.persistentVolumeClaim.tpl" -}}
metadata:
  name: {{ .name }}
spec:
  resources: {{ toYaml .resources | nindent 4 }}
{{- end -}}

{{- /*
Wrapper for utils.yaml.merge for rendering a kind of resource.

Covers the common use case of merging the common template for a resource with
one that is populated with the given properties dervived from .Values.islandora.



Additional templates can be specified as well.

Usage:
{{ include "islandora.common.merge" (dict
    "kind" "RESOURCE_NAME"
    "properties" (dict "name" RESOURCE_NAME ...)
    "context" $
    "templates" (include "TEMPLATE_N") ...)
}}
*/ -}}
{{- define "islandora.common.merge" -}}
{{ include "utils.yaml.merge"
  (concat
    (list
      (include (printf "common.%s.tpl" .kind) .context)
      (include (printf "islandora.%s.tpl" .kind) (set (deepCopy .properties) "context" .context)))
    (.templates | default list))
}}
{{- end -}}

{{- /*
Wrappers for islandora.common.merge.
*/ -}}
{{- define "islandora.configMap" -}}
---
{{ include "islandora.common.merge" (set (deepCopy .) "kind" "configMap") }}
{{ end }}

{{- define "islandora.deployment" -}}
---
{{ include "islandora.common.merge" (set (deepCopy .) "kind" "deployment") }}
{{ end }}

{{- define "islandora.service" -}}
---
{{ include "islandora.common.merge" (set (deepCopy .) "kind" "service") }}
{{ end }}

{{- define "islandora.statefulSet" -}}
---
{{ include "islandora.common.merge" (set (deepCopy .) "kind" "statefulSet") }}
{{ end }}

{{- define "islandora.persistentVolumeClaim" -}}
---
{{ include "islandora.common.merge" (set (deepCopy .) "kind" "persistentVolumeClaim") }}
{{ end }}
