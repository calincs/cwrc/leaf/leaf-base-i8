{{- /* vim: set filetype=mustache: */ -}}

{{- /*
Metadata labels that apply to all resources.

See: https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/
See: https://helm.sh/docs/chart_best_practices/labels/
See: https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/blob/v2.37.0/assets/auto-deploy-app/templates/_helpers.tpl#L73-85
*/ -}}
{{- define "common.metadata.labels.tpl" -}}
{{ include "sharedlabels" . }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end -}}

{{- /*
Metadata annotations used by GitLab, to support Deploy boards.

See: https://docs.gitlab.com/ee/user/project/deploy_boards.html.
*/ -}}
{{- define "common.annotations.gitlab.tpl" -}}
{{ if ((.Values.gitlab).app) }}app.gitlab.com/app: {{ .Values.gitlab.app | quote }}{{ end }}
{{ if ((.Values.gitlab).env) }}app.gitlab.com/env: {{ .Values.gitlab.env | quote }}{{ end }}
{{- end -}}

{{- /*
Permits access to secrets provided by GitLab.

See: https://docs.gitlab.com/ee/topics/autodevops/customize.html#application-secret-variables
*/ -}}
{{- define "common.annotations.checksum.tpl" -}}
{{- if ((.Values.application).secretChecksum) -}}checksum/application-secrets: {{ .Values.application.secretChecksum | quote }}{{- end -}}
{{- end -}}

{{- /*
See https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.25/#labelselector-v1-meta
*/ -}}
{{- define "common.labelSelector.matchLabels.tpl" -}}
app: {{ template "appname" . }}
release: {{ .Release.Name }}
tier: {{ .Values.application.tier }}
track: {{ .Values.application.track }}
{{- end -}}

{{- /*
See: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.25/#podtemplatespec-v1-core
*/ -}}
{{- define "common.podTemplateSpec.tpl" -}}
metadata:
  labels: {{ include "utils.yaml.merge" (list
    (include "common.metadata.labels.tpl" .)
    (include "common.labelSelector.matchLabels.tpl" .)) | nindent 4 }}
  annotations: {{ include "utils.yaml.merge" (list
    (include "common.annotations.gitlab.tpl" .)
    (include "common.annotations.checksum.tpl" .)) | nindent 4 }}
spec:
  enableServiceLinks: false
  affinity:
    podAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 100
        podAffinityTerm:
          labelSelector:
            matchExpressions:
            - key: "app"
              operator: In
              values:
              - {{ template "appname" . }}
          topologyKey: "kubernetes.io/hostname"
  terminationGracePeriodSeconds: 10
{{- if hasKey .Values.image "secrets" }}
  imagePullSecrets: {{ toYaml .Values.image.secrets | nindent 2 }}
{{- end }}
{{- end -}}

{{- /*
Resource templates with common defaults.
*/ -}}

{{- /*
See: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.25/#configmap-v1-core
*/ -}}
{{- define "common.configMap.tpl" -}}
apiVersion: v1
kind: ConfigMap
metadata:
  labels: {{ include "common.metadata.labels.tpl" . | nindent 4 }}
{{- end -}}

{{- /*
See: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.25/#deployment-v1-apps
*/ -}}
{{- define "common.deployment.tpl" -}}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels: {{ include "utils.yaml.merge" (list
    (include "common.metadata.labels.tpl" $)
    (include "common.labelSelector.matchLabels.tpl" .)) | nindent 4 }}
  annotations: {{ include "common.annotations.gitlab.tpl" . | nindent 4 }}
spec:
  selector:
    matchLabels: {{ include "common.labelSelector.matchLabels.tpl" . | nindent 6 }}
  template: {{ include "common.podTemplateSpec.tpl" . | nindent 4 }}
{{- end -}}

{{- /*
See: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.25/#service-v1-core
*/ -}}
{{- define "common.service.tpl" -}}
---
apiVersion: v1
kind: Service
metadata:
  labels: {{ include "common.metadata.labels.tpl" . | nindent 4 }}
    track: {{ .Values.application.track }}
  annotations:
{{- if .Values.prometheus.metrics }}
    prometheus.io/scrape: "true" # No port specified as islandora services often use many ports.
{{- end }}
spec:
  type: "ClusterIP"
  selector: {{ include "common.labelSelector.matchLabels.tpl" . | nindent 4 }}
{{- end -}}

{{- /*
See: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.25/#statefulset-v1-apps
*/ -}}
{{- define "common.statefulSet.tpl" -}}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels: {{ include "utils.yaml.merge" (list
    (include "common.metadata.labels.tpl" $)
    (include "common.labelSelector.matchLabels.tpl" .)) | nindent 4 }}
  annotations: {{ include "common.annotations.gitlab.tpl" . | nindent 4 }}
spec:
  selector:
    matchLabels: {{ include "common.labelSelector.matchLabels.tpl" . | nindent 6 }}
  template: {{ include "common.podTemplateSpec.tpl" . | nindent 4 }}
{{- end -}}

{{- /*
See: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.25/#persistentvolumeclaim-v1-core
*/ -}}
{{- define "common.persistentVolumeClaim.tpl" -}}
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  labels: {{ include "common.metadata.labels.tpl" . | nindent 4 }}
spec:
  accessModes:
    - ReadWriteOnce
{{- end -}}
