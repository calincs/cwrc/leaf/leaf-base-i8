# Default values for chart.

# Should match ISLE_* values in the '.env' file in the root
# directory of this project.
isle:
  imagePullPolicy: IfNotPresent
  repository: islandora
  tag: 3.4.4

# Defaults for all components.
container:
  resources:
    # limits:
    #   cpu: 100m
    #   memory: 128Mi
    requests:
      cpu: 100m
      memory: 500Mi

# Defaults for all components.
volume:
  resources:
    requests:
      storage: 2Gi

# Islandora properties.
islandora:
  # Default properties for islandora, used in deployments etc.
  # Templates values are used such that the defaults for all components can be
  # overridden and/or each values can be given explicitly for each component.
  defaults:
    containers:
      default: &default-container
        resources:
          requests:
            cpu: "{{ .Values.container.resources.requests.cpu }}"
            memory: "{{ .Values.container.resources.requests.memory }}"
    volumes:
      default: &default-volume
        # If you want to use a custom storage class, you can specify `storageClass`. In this case, you need to create a storage class at first.
        # If `storageClass` is not specified, the behavior will vary per Kubernetes provider.
        # For example, GKE automatically chooses a default storage class and allocate a physical storage.
        # See https://cloud.google.com/kubernetes-engine/docs/concepts/persistent-volumes for more information.
        # storageClass: "myStorage"
        resources:
          requests:
            storage: "{{ .Values.volume.resources.requests.storage }}"
    workloads:
      default: &default-workloads
        replicas: 1
    images:
      isle-image: &default-isle-image
        imagePullPolicy: "{{ .Values.isle.imagePullPolicy }}"
        repository: "{{ .Values.isle.repository }}"
        tag: "{{ .Values.isle.tag }}"
      gitlab-image: &default-gitlab-image
        imagePullPolicy: "{{ .Values.image.pullPolicy }}"
        repository: "{{ .Values.image.repository }}"
        tag: "{{ .Values.image.tag }}"
    configs:
      default: &default-config {}
      php: &default-php-config
        NGINX_CLIENT_BODY_TIMEOUT: "3600"
        NGINX_FASTCGI_CONNECT_TIMEOUT: "3600"
        NGINX_FASTCGI_READ_TIMEOUT: "3600"
        NGINX_FASTCGI_SEND_TIMEOUT: "3600"
        NGINX_KEEPALIVE_TIMEOUT: "3600"
        NGINX_LINGERING_TIMEOUT: "3600"
        NGINX_PROXY_CONNECT_TIMEOUT: "3600"
        NGINX_PROXY_READ_TIMEOUT: "3600"
        NGINX_PROXY_SEND_TIMEOUT: "3600"
        NGINX_SEND_TIMEOUT: "3600"
        PHP_DEFAULT_SOCKET_TIMEOUT: "3600"
        PHP_MAX_EXECUTION_TIME: "3600"
        PHP_MAX_FILE_UPLOADS: "3600"
        PHP_MAX_INPUT_TIME: "120"
        PHP_MEMORY_LIMIT: "2048M"
        PHP_POST_MAX_SIZE: "1024M"
        PHP_PROCESS_CONTROL_TIMEOUT: "360"
        PHP_REQUEST_TERMINATE_TIMEOUT: "360"
        PHP_UPLOAD_MAX_FILESIZE: "1024M"
  volumes:
    activemq-data:
      <<: [*default-volume]
    cantaloupe-data:
      <<: [*default-volume]
    drupal-public-files:
      <<: [*default-volume]
    drupal-private-files:
      <<: [*default-volume]
    mariadb-data:
      <<: [*default-volume]
    solr-data:
      <<: [*default-volume]
  configs:
    activemq:
      <<: [*default-config]
    alpaca:
      <<: [*default-config]
      ALPACA_FITS_TIMEOUT: "300000"
      ALPACA_HOMARUS_TIMEOUT: "300000"
      ALPACA_HOUDINI_TIMEOUT: "300000"
      ALPACA_OCR_TIMEOUT: "300000"
    cantaloupe:
      <<: [*default-config]
    crayfits:
      <<: [*default-config, *default-php-config]
    drupal:
      <<: [*default-config, *default-php-config]
    fits:
      <<: [*default-config]
    homarus:
      <<: [*default-config, *default-php-config]
    houdini:
      <<: [*default-config, *default-php-config]
      HOUDINI_PHP_MEMORY_LIMIT: "1024M"
      HOUDINI_PHP_POST_MAX_SIZE: "1024M"
      HOUDINI_PHP_UPLOAD_MAX_FILESIZE: "1024M"
      HOUDINI_PHP_MAX_EXECUTION_TIME: "300"
    hypercube:
      <<: [*default-config, *default-php-config]
    mariadb:
      <<: [*default-config]
    matomo:
      <<: [*default-config, *default-php-config]
    milliner:
      <<: [*default-config, *default-php-config]
    solr:
      <<: [*default-config]
      SOLR_JAVA_OPTS: "-Dsolr.data.home=/data"
  containers:
    activemq:
      <<: [*default-isle-image, *default-container]
    alpaca:
      <<: [*default-isle-image, *default-container]
    cantaloupe:
      <<: [*default-isle-image, *default-container]
    crayfits:
      <<: [*default-isle-image, *default-container]
    drupal:
      <<: [*default-gitlab-image, *default-container]
    fits:
      <<: [*default-isle-image, *default-container]
    homarus:
      <<: [*default-isle-image, *default-container]
    houdini:
      <<: [*default-isle-image, *default-container]
    hypercube:
      <<: [*default-isle-image, *default-container]
    mariadb:
      <<: [*default-isle-image, *default-container]
    matomo:
      <<: [*default-isle-image, *default-container]
    milliner:
      <<: [*default-isle-image, *default-container]
    solr:
      <<: [*default-gitlab-image, *default-container]
  workloads:
    activemq:
      <<: *default-workloads
    alpaca:
      <<: *default-workloads
    cantaloupe:
      <<: *default-workloads
    crayfits:
      <<: *default-workloads
    drupal:
      <<: *default-workloads
    fits:
      <<: *default-workloads
    homarus:
      <<: *default-workloads
    houdini:
      <<: *default-workloads
    hypercube:
      <<: *default-workloads
    mariadb:
      <<: *default-workloads
    matomo:
      <<: *default-workloads
    milliner:
      <<: *default-workloads
    solr:
      <<: *default-workloads

# The following values are provided by GitLab as part of the auto-deploy tool:
# https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image.
#
# Defaults are provided here for the purposes of local testing.
#
# Values relating to the following features are excluded as we do not make
# use of them:
#
# - PostgreSQL database support https://docs.gitlab.com/ee/topics/autodevops/customize.html#postgresql-database-support
# - CI_APPLICATION_* https://docs.gitlab.com/ee/topics/autodevops/customize.html#custom-container-image
# - Canary environments https://docs.gitlab.com/ee/topics/autodevops/customize.html#deploy-policy-for-canary-environments
# - Replica count https://docs.gitlab.com/ee/topics/autodevops/customize.html#customize-values-for-helm-chart
# - ModSecurity https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/blob/master/doc/api.md
# - Prometheus https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#manual-configuration-of-prometheus
#
# Excluded values:
#
# - application.database_url
# - application.initializeCommand
# - image.repository
# - image.tag
# - ingress.canary.*
# - ingress.modSecurity.*
# - postgresql.*
# - prometheus.metrics
# - replicaCount
#
# See: https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/blob/v2.37.0/src/bin/auto-deploy#L362-393
# For how these values are specified by the CI/CD deploy job.
#
# See: https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/tree/v2.37.0/assets/auto-deploy-app#configuration
# For descriptions of the values.
application:
  # https://docs.gitlab.com/ee/user/project/canary_deployments.html
  # https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html
  # https://docs.gitlab.com/ee/topics/autodevops/customize.html#advanced-replica-variables-setup
  track: stable
  tier: web
  # K8S_SECRET_* https://docs.gitlab.com/ee/topics/autodevops/customize.html#application-secret-variables
  # See .gitlab-ci.yml for the secrets.
  secretName: leaf-secret # Prefixed with the same values as set for releaseOverride and track (if not stable), always suffixed with "-secret".
  secretChecksum: # Generated by https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/tree/v2.37.0/src/bin/auto-deploy-application-secrets-yaml

image:
  # Should match REPOSITORY and TAG values in the '.env' file in the root
  # directory of this project.
  repository: registry.gitlab.com/calincs/cwrc/leaf/leaf-base-i8 # CI_APPLICATION_REPOSITORY
  tag: latest # CI_APPLICATION_TAG
  pullPolicy: IfNotPresent
  secrets:
    - name: gitlab-registry

extraLabels: {}

gitlab:
  app: # CI_PROJECT_PATH_SLUG
  env: # CI_ENVIRONMENT_SLUG
  envName: # CI_ENVIRONMENT_NAME
  envURL: # CI_ENVIRONMENT_URL
  projectID: # CI_PROJECT_ID

# Populated by the environment variable HELM_RELEASE_NAME if present otherwise CI_ENVIRONMENT_SLUG.
releaseOverride:

service:
  url: islandora.dev # CI_ENVIRONMENT_URL
  additionalHosts: # ADDITIONAL_HOSTS https://docs.gitlab.com/ee/topics/autodevops/customize.html#build-and-deployment
  commonName: # le-$CI_PROJECT_ID.$KUBE_INGRESS_BASE_DOMAIN

# Assumes cert-manager is installed in the cluster and configured correctly.
ingress:
  expose:
    solr: false
    activemq: false
  annotations:
    # GitLab does not provide annotations by default, these are specific to leaf.
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/proxy-body-size: "500m"
    nginx.ingress.kubernetes.io/proxy-buffer-size: "128k"
    nginx.ingress.kubernetes.io/proxy-buffering: "on"
    nginx.ingress.kubernetes.io/proxy-buffers-number: "4"
    nginx.ingress.kubernetes.io/proxy-connect-timeout: "300"
    nginx.ingress.kubernetes.io/proxy-read-timeout: "300"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "300"
  enabled: true
  tls:
    enabled: true
    acme: true
  # className: nginx
  modSecurity:
    enabled: false
    secRuleEngine: "DetectionOnly"
    # secRules:
    #   - variable: ""
    #     operator: ""
    #     action: ""
  canary:
    weight:

prometheus:
  metrics: false
# End GitLab specific values.
